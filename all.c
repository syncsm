/*
 * this code is protected by the GNU affero GPLv3 license
 * author:Sylvain BERTRAND
 */
#include "config.h"
/*----------------------------------------------------------------------------*/
#include "ulinux/utils/qsort.c"
#include "ulinux/utils/mem.c"
#include "ulinux/utils/ascii/block/conv/net/net.c"
#include "ulinux/utils/ascii/block/conv/hexadecimal/hexadecimal.c"
#include "ulinux/utils/ascii/string/vsprintf.c"
/*----------------------------------------------------------------------------*/
#include "syncsm.c"
/*----------------------------------------------------------------------------*/
#include "dns/resolver.c"
#ifdef CONFIG_DNS_IPV4
#include "dns/ipv4.c"
#endif
#include "dns/ipv6.c"
#include "dns/query.c"
#include "dns/response.c"
#include "dns/name.c"
#include "dns/resolv_conf.c"
/*----------------------------------------------------------------------------*/
#include "smtp/smtp.c"
