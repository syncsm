#ifndef CONFIG_H
#define CONFIG_H
/*
 * this code is protected by the GNU affero GPLv3 license
 * author:Sylvain BERTRAND
 */
/*
 * The originating email address:
 * CONFIG_SMTP_MAIL_FROM@CONFIG_SMT_EHLO_DN_AL
 */
/* #define CONFIG_SMTP_MAIL_FROM "sylvain.bertrand"
   #define CONFIG_SMTP_EHLO_DN_AL "[x.x.x.x]"
   #define CONFIG_SMTP_EHLO_DN_AL "[IPV6:....]" */
/*----------------------------------------------------------------------------*/
/*
 * define the following macro if you want to handle smtp transparency
 * manually, namely you will to escape yourself "<CRLF>.<CRLF>"
 */ 
/* #define CONFIG_DISABLE_SMTP_TRANSPARENCY 1 */
/*----------------------------------------------------------------------------*/
/* KEEP AN EYE ON ABBREVIATIONS */
/* legacy: will query ipv4 DNS servers */
#define CONFIG_DNS_IPV4 1	
/* legacy: will query DNS for smtp IPv4 servers and use them before IPv6 ones */
#define CONFIG_SMTP_IPV4 1
/*----------------------------------------------------------------------------*/
/* the amount of address space which will be booked in our process */
#define CONFIG_EMAIL_ADDRESS_SPACE (100 * 1000 * 1000)
/*----------------------------------------------------------------------------*/
/*
 * sending the same email to 32 ips/domain names is already insane for the
 * target scale of this tool
 */
#define CONFIG_DN_AL_N_MAX 32 
/*----------------------------------------------------------------------------*/
#define CONFIG_DN_SMTP_IPS_N_MAX 32 /* max 32 smtp server IPs per domain name */
/* kind of worst case, all above ips are each from an unique mx */
#define CONFIG_MXS_N_MAX CONFIG_DN_SMTP_IPS_N_MAX
/*----------------------------------------------------------------------------*/
/* where is the resolv.conf file for nameservers */
#define CONFIG_RESOLV_CONF_PATH "/etc/resolv.conf"
/*----------------------------------------------------------------------------*/
/* a "good enough" generic buffer size (2 pages) */
#define CONFIG_BUFSIZ 8192
/*----------------------------------------------------------------------------*/
#define CONFIG_DNS_NSS_N_MAX 3				/* max name servers */
#define CONFIG_DNS_NS_RESP_WAIT_TIME_SEC	4	/* second  */
#define CONFIG_DNS_NS_RESP_WAIT_TIME_NSEC	0	/* nanosecond */
/*----------------------------------------------------------------------------*/
#define CONFIG_DNS_LABEL_PTR_DEPTH_MAX 128
/*----------------------------------------------------------------------------*/
#define CONFIG_SMTP_REPLY_LINES_N_MAX 16
/*----------------------------------------------------------------------------*/
#endif
