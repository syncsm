#ifndef SYNCSM_DNS_H
#define SYNCSM_DNS_H
/*
 * this code is protected by the GNU affero GPLv3 license
 * author:Sylvain BERTRAND
 */
#include "namespace/dns.h"
/*----------------------------------------------------------------------------*/
static void dns_init(void);
static void dns_resolver(void);
/*----------------------------------------------------------------------------*/
#define CLEANUP
#include "namespace/dns.h"
#undef CLEANUP
#endif
