#ifndef SYNCSM_DNS_IPV4_C
#define SYNCSM_DNS_IPV4_C
/*
 * this code is protected by the GNU affero GPLv3 license
 * author:Sylvain BERTRAND
 */
/*----------------------------------------------------------------------------*/
/* compiler stuff */
#include <stdbool.h>
#include <stdarg.h>
/*----------------------------------------------------------------------------*/
#include "config.h"
/*----------------------------------------------------------------------------*/
#include "ulinux.h"
/*----------------------------------------------------------------------------*/
#include "dns/rfc.h"
#include "smtp/rfc.h"
/*----------------------------------------------------------------------------*/
#include "perr.h"
#include "syncsm.h"
/*----------------------------------------------------------------------------*/
#include "dns/state.h"
#include "dns/resolver.h"
/*============================================================================*/
#include "namespace/ulinux.h"
#include "dns/namespace/state.h"
#include "dns/namespace/resolver.h"
#include "dns/namespace/ipv4.h"
#include "dns/namespace/ipv4.c"
/*----------------------------------------------------------------------------*/
static si udp_so;
static struct sockaddr_in sa;
/*----------------------------------------------------------------------------*/
static bool qtion_send_ns(void)
{
	sl r;
	u16 sent_bytes_n;

	sent_bytes_n = 0;
	loop {
		r = write(udp_so, query_v + sent_bytes_n,
						query_sz_v - sent_bytes_n);
		if (ISERR(r)) {
			if ((r == -EAGAIN) || (r == -EINTR))
				continue;
			PERR("1:DNS:IPV4:WARNING:failed to send question to ipv4 name server 0x%08x for domain \"%.*s\"\n", be32_to_cpu(nss_v[ns_v].ipv4_net), dn_v->dn.l - dn_v->dn.s + 1, dn_v->dn.s); 
			return false;
		}

		sent_bytes_n += (u16)r;
		if (sent_bytes_n == query_sz_v) {
			PERR("i1:DNS:IPV4:sent %d bytes to ns 0x%08x\n", query_sz_v, be32_to_cpu(nss_v[ns_v].ipv4_net));
			return true;
		}
	}
}

static void sa_init_ns(void)
{
	memset(&sa, 0, sizeof(sa));

	sa.family = AF_INET;
	sa.port = cpu_to_be16(DNS_RFC_PORT);
	memcpy(&sa.addr, &nss_v[ns_v].ipv4_net, sizeof(nss_v[ns_v].ipv4_net));
}

static void connect_ns(void)
{
	sl r;

	r = connect(udp_so, &sa, sizeof(sa));
	if (ISERR(r)) {
		if ((r != -EAGAIN) && (r != -EINPROGRESS) && (r != -EINTR)) {
			PERR("0:DNS:IPV4:ERROR:%ld:unable to configure socket with the name server ipv4 address 0x%08x\n", r, be32_to_cpu(sa.addr));
			exit(1);
		}
	}
}

static bool timer_expired(void)
{
	sl r;
	u64 expirations_n; /* the count of expirations of our timer */

	expirations_n = 0;

	loop {/* reads are atomic or err, aka no short reads */
		r = read(timerfd_v, &expirations_n, sizeof(u64));
		if (r != -EINTR);
			break;
	}

	if (ISERR(r)) {
		PERR("0:DNS:IPV4:ERROR:TIMER:%ld:unable reading the count of expirations\n", r);
		exit(1);
	}
	PERR("1:DNS:IPV4:TIMER:count of expirations=%lu for name server 0x%08x\n", expirations_n, be32_to_cpu(nss_v[ns_v].ipv4_net));
	if (expirations_n != 0)
		return true;
	return false;
}

static void udp_datagram_read(void) 
{
	sl  r;
	loop {
		/* atomically get 1 message */
		r = recvfrom(udp_so, resp_v, DNS_RFC_UDP_PAYLOAD_SZ_MAX, 0 ,0
									,0);
		if (r == -EAGAIN  || r == -EINTR)
			continue;
		if (!ISERR(r))
			break;
		PERR("0:DNS:IPV4:ERROR:%ld:error while reading the udp datagram\n", r);
		exit(1);
	}
	resp_sz_v = (u16)r;
	resp_e_v = resp_v + resp_sz_v;
	PERR("1:DNS:RESPONSE:[0x%p-0x%p]\n", resp_v, resp_e_v - 1);
}

/* copy/paste/adapt from ulinux pattern epoll_timer */
static bool ns_resp_wait(void)
{
	loop {
		/*
		 * 2 events if the timer do happen at the same time that a udp
		 * datagram is received
		 */
		struct epoll_event evts[2];
		sl evt;
		sl r;

		loop {		
			memset(evts, 0, sizeof(evts));
			r = epoll_pwait(epfd_v, evts, 2, -1, 0);
			if (r != -EINTR)
				break;
			PERR("1:DNS:IPV4:WARNING:epoll_pwait was interruped by a signal, restarting\n");
		}
		if (ISERR(r)) {
			PERR("0:DNS:IPV4:ERROR:%ld:epoll_wait error\n", r);
			exit(1);
		}

		if (r == 0) /* meh... */
			continue;

		/* FIRST: probe for a udp datagram arrival event */	
		evt = 0;
		loop {
			if (evt == r)
				break;
			if (evts[evt].data.fd == udp_so) {
				if ((evts[evt].events & EPOLLIN) != 0) {
					udp_datagram_read();
					return true;
				} else {
					PERR("0:DNS:IPV4:ERROR:got unselected event for ipv4_udp, events 0x%08x\n", evts[evt].events);
					exit(1);
				}
			}
			++evt;
		}

		/* THEN: check the timer expiration */
		evt = 0;
		loop {
			if (evt == r)
				break;

			if (evts[evt].data.fd == timerfd_v) {
				if ((evts[evt].events & EPOLLIN) != 0) {
					if (timer_expired())
						return false;
				} else {
					PERR("0:DNS:IPv4:ERROR:got unselected event for the timer, events 0x%08x\n", evts[evt].events);
					exit(1);
				}
			}
			++evt;
		}
	}
}
/******************************************************************************/
/* exported in ipv4.h */
static void ipv4_udp_so_setup(void)
{
	sl r;
	struct epoll_event evts;

	/* ipv4 */
	r = socket(AF_INET, SOCK_O_NONBLOCK | SOCK_DGRAM, 0);
	if (ISERR(r)) {
		PERR("0:DNS:IPV4:ERROR:%ld:unable to create ipv4 udp socket for dns\n", r);
		exit(1);
	}
	udp_so = (si)r;
	
	memset(&evts, 0, sizeof(evts));
	evts.events = EPOLLIN;
	evts.data.fd = udp_so;
	r = epoll_ctl(epfd_v, EPOLL_CTL_ADD, udp_so, &evts);
	if (ISERR(r)) {
		PERR("0:DNS:IPV4:ERROR:%ld:unable to add the ipv4 socket file descriptor to the epoll file descriptor\n", r);
		exit(1);
	}
}

static bool ipv4_ns(void)
{
	sa_init_ns();
	connect_ns();

	if (!qtion_send_ns())
		return false;
	return ns_resp_wait();
}
/*-----------------------------------------------------------------------------*/
#define CLEANUP
#include "namespace/ulinux.h"
#include "dns/namespace/state.h"
#include "dns/namespace/resolver.h"
#include "dns/namespace/ipv4.h"
#include "dns/namespace/ipv4.c"
#undef CLEANUP
#endif
