#ifndef SYNCSM_DNS_IPV4_H
#define SYNCSM_DNS_IPV4_H
#include "dns/namespace/ipv4.h"
/*
 * this code is protected by the GNU affero GPLv3 license
 * author:Sylvain BERTRAND
 */
static void ipv4_udp_so_setup(void);
static bool ipv4_ns(void);
#define CLEANUP
#include "dns/namespace/ipv4.h"
#undef CLEANUP
#endif
