#ifndef SYNCSM_DNS_IPV6_H
#define SYNCSM_DNS_IPV6_H
#include "dns/namespace/ipv6.h"
/*
 * this code is protected by the GNU affero GPLv3 license
 * author:Sylvain BERTRAND
 */
static void ipv6_udp_so_setup(void);
static bool ipv6_ns(void);
#define CLEANUP
#include "dns/namespace/ipv6.h"
#undef CLEANUP
#endif
