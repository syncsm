#ifndef SYNCSM_DNS_NAME_C
#define SYNCSM_DNS_NAME_C
/*
 * this code is protected by the GNU affero GPLv3 license
 * author:Sylvain BERTRAND
 */
/* compiler stuff */
#include <stdbool.h>
#include <stdarg.h>
/*----------------------------------------------------------------------------*/
#include "config.h"
/*----------------------------------------------------------------------------*/
#include "ulinux.h"
/*----------------------------------------------------------------------------*/
#include "dns/rfc.h"
#include "smtp/rfc.h"
/*----------------------------------------------------------------------------*/
#include "perr.h"
#include "syncsm.h"
/*============================================================================*/
#include "namespace/ulinux.h"
#include "namespace/syncsm.h"
#include "dns/namespace/name.h"
#include "dns/namespace/name.c"
/*----------------------------------------------------------------------------*/
#define IS_PTR(x) \
(((x) & DNS_RFC_LABEL_OFFSET_IS_PTR_MARK) == DNS_RFC_LABEL_OFFSET_IS_PTR_MARK)
/*----------------------------------------------------------------------------*/
static void label_ptr(u8 **label, u8 *msg_s, u8 *msg_e, u8 *depth)
{
	if ((*label) >= msg_e) {
		PERR("0:DNS:LABEL_PTR:ERROR:the label pointer is out of bounds\n");
		exit(1);
	}

	if (IS_PTR((*label)[0])) {
		u16 offset;

		if (*depth == 0) {
			PERR("0:DNS:LABEL_PTR:ERROR:max label pointer recursion reached, %u\n", CONFIG_DNS_LABEL_PTR_DEPTH_MAX);
			exit(1);
		}
		(*depth)--;

		if ((*label + 1) >= msg_e) {
			PERR("0:DNS:LABEL_PTR:ERROR:the label pointer offset is itself out of bounds\n");
			exit(1);
		}

		offset = be16_to_cpu(*(u16*)(*label));
		offset &= DNS_RFC_LABEL_PTR_OFFSET_MASK;

		PERR("1:DNS:LABEL_PTR:offset 0x%04x [0x%p-0x%p]\n",  offset, msg_s, msg_e - 1);
		*label = msg_s + offset;
	}
}

/*
 * return a pointer on the byte following the last valid byte of the destination
 * label
 */
static u8 *label_cpy(u8 *d, u8 *d_e, u8* s, u8* s_msg_e)
{
	u8 sz;
	u8 *d_l; /* last byte */
	u8 *s_l; /* last byte */

	PERR("1:DNS:LABEL_CPY:START\n");
	if (d >= d_e) {
		PERR("0:DNS:LABEL_CPY:ERROR:D:label is out of bounds\n");
		exit(1);
	}

	if (s >= s_msg_e) {
		PERR("0:DNS:LABEL_CPY:ERROR:S:label is out of bounds\n");
		exit(1);
	}

	d[0] = s[0];

	sz = s[0];
	if (sz == 0) {
		PERR("1:DNS:LABEL_CPY:END:EQ:zero size\n");
		return d + 1;
	}

	d_l = d + 1 + sz - 1;
	if (d_l >= d_e) {
		PERR("0:DNS:LABEL_CPY:ERROR:D:label will go out of bounds\n");
		exit(1);
	}

	s_l = s + 1 + sz -1;
	if (s_l >= s_msg_e) {
		PERR("0:DNS:LABEL_CPY:ERROR:S:label is going out of bounds\n");
		exit(1);
	}
		
	PERR("1:DNS:LABEL_CPY:S:B:%.*s(%u)\n", sz, s + 1, sz);
	memcpy(d + 1, s + 1, sz);
	PERR("1:DNS:LABEL_CPY:END\n");
	return d + 1 + sz;
}

static bool label_cmp(u8 *a, u8 *a_msg_e, u8* b, u8* b_msg_e)
{
	u8 sz;
	u8 *a_l;
	u8 *b_l;

	PERR("1:DNS:LABEL_CMP:START\n");
	if (a >= a_msg_e) {
		PERR("0:DNS:LABEL_CMP:ERROR:A:label is out of bounds\n");
		exit(1);
	}

	if (b >= b_msg_e) {
		PERR("0:DNS:LABEL_CMP:ERROR:B:label is out of bounds\n");
		exit(1);
	}

	if (a[0] != b[0]) {
		PERR("1:DNS:LABEL_CMP:END:NEQ:not the same size\n");
		return false;
	}

	sz = a[0];
	if (sz == 0) {
		PERR("1:DNS:LABEL_CMP:END:EQ:zero size\n");
		return true;
	}

	a_l = a + 1 + sz - 1;
	if (a_l >= a_msg_e) {
		PERR("0:DNS:LABEL_CMP:ERROR:A:label is going out of bounds\n");
		exit(1);
	}

	b_l = b + 1 + sz -1;
	if (b_l >= b_msg_e) {
		PERR("0:DNS:LABEL_CMP:ERROR:B:label is going out of bounds\n");
		exit(1);
	}
		
	PERR("1:DNS:LABEL_CMP:A:%.*s(%u):B:%.*s(%u)\n", a[0], a + 1, a[0], b[0], b + 1, b[0]);
	if (!memcmp(a + 1, b + 1, sz)) {
		PERR("1:DNS:LABEL_CMP:END:NEQ\n");
		return false;
	}
	PERR("1:DNS:LABEL_CMP:END:EQ\n");
	return true;
}
/******************************************************************************/
/* the following until the end of the file is exported in name.h */
/*
 * build a "dns name". Good Enough.
 * return a pointer on the byte following the name.
 * a domain name string in smtp must not end with a dot. 
 */
static u8 *name_build(u8 *name, struct str_slice_t *dn)
{
	u8 *label_s;
	u8 *label_l;
	u8 idx;
	bool last_label;
	u64 name_sz;

	label_s = dn->s;
	label_l = dn->s;
	last_label = false;
	idx = 0;
	name_sz = 0;
	loop {
		if (label_l == dn->l)
			last_label = true;

		if (last_label || (label_l[1] == '.')) {
			u64 label_sz;

			label_sz = label_l - label_s + 1;
			if (label_sz > DNS_RFC_LABEL_SZ_MAX) {
				PERR("0:DNS:NAME_BUILDER:ERROR:dns label length greater than rfc limits, max is %u, faulty dns label is %.*s\n", DNS_RFC_LABEL_SZ_MAX, label_sz, label_s);
				exit(1);
			} 
			name[0] = (u8)label_sz;
			name_sz += 1 + label_sz;
			if (name_sz  > DNS_RFC_NAME_SZ_MAX) {
				PERR("0:DNS:ERROR:NAME_BUILDER:dns name length greater than rfc limits, max is %u, faulty dns name is %.*s\n", DNS_RFC_NAME_SZ_MAX, dn->l - dn->s + 1, dn->s);
				exit(1);
			}
			memcpy(name + 1, label_s, label_sz);

			PERR("1:DNS:NAME_BUILDER:LABEL:\"%.*s\"[%u]:%u:%.*s\n", dn->l - dn->s + 1, dn->s, idx, label_sz, label_sz, label_s);
			++idx;

			if (last_label) {
				name += 1 + label_sz;

				name_sz += 1; /* the "null" root domain name, only the length octet which is 0 */
				if (name_sz  > DNS_RFC_NAME_SZ_MAX) {
					PERR("0:DNS:ERROR:NAME_BUILDER:dns name length greater than rfc limits, cannot fit the null domain, max is %u, faulty dns name is %.*s\n", DNS_RFC_NAME_SZ_MAX, dn->l - dn->s + 1, dn->s);
					exit(1);
				}
				name[0] = 0; /* the 'null'/root label */
				return name + 1;
			}

			label_s = label_l + 2; /* skip '.' */
			label_l = label_s;

			name += 1 + label_sz;
		} else
			++label_l;
	}
}

/*
 * decompress/copy a source name from a msg into a buffer.
 * will return a pointer on the byte past the last copy byte.
 */
static u8 *name_cpy(u8 *d, u8 *s, u8 *s_msg_s, u8 *s_msg_e)
{
	u8 *s_label;
	u8 s_depth;
	
	PERR("1:DNS:NAME_CPY:START\n");
	PERR("1:DNS:NAME_CPY:D:0x%p[DNS_RFC_NAME_SZ_MAX=0x%x]\n", d, DNS_RFC_NAME_SZ_MAX);
	PERR("1:DNS:NAME_CPY:S:0x%p[0x%p-0x%p]\n", s, s_msg_s, s_msg_e - 1);

	if (s < s_msg_s || s_msg_e <= s) {
		PERR("0:DNS:NAME_CPY:ERROR:S:name start is out of bounds\n");
		exit(1);
	}

	s_label = s;
	s_depth = CONFIG_DNS_LABEL_PTR_DEPTH_MAX;;
	loop {
		u8 label_sz;

		label_ptr(&s_label, s_msg_s, s_msg_e, &s_depth);

		d = label_cpy(d, d + DNS_RFC_NAME_SZ_MAX, s_label, s_msg_e);

		label_sz = s_label[0];

		if (label_sz == 0) {
			PERR("1:DNS:NAME_CPY:END\n");
			return d;
		}

		s_label += label_sz + 1;
	}
}

static bool name_cmp(u8 *a, u8 *a_msg_s, u8* a_msg_e, u8 *b, u8 *b_msg_s,
								u8* b_msg_e)
{
	u8 *a_label;
	u8 *b_label;
	u8 a_depth;
	u8 b_depth;
	
	PERR("1:DNS:NAME_CMP:START\n");
	PERR("1:DNS:NAME_CMP:A:0x%p[0x%p-0x%p]\n", a, a_msg_s, a_msg_e - 1);
	PERR("1:DNS:NAME_CMP:B:0x%p[0x%p-0x%p]\n", b, b_msg_s, b_msg_e - 1);

	if (a < a_msg_s || a_msg_e <= a) {
		PERR("0:DNS:NAME_CMP:ERROR:A:name start is out of bounds\n");
		exit(1);
	}
	if (b < b_msg_s || b_msg_e <= b) {
		PERR("0:DNS:NAME_CMP:ERROR:B:name start is out of bounds\n");
		exit(1);
	}

	a_label = a;
	b_label = b;
	a_depth = CONFIG_DNS_LABEL_PTR_DEPTH_MAX;
	b_depth = CONFIG_DNS_LABEL_PTR_DEPTH_MAX;;
	loop {
		u8 label_sz;

		label_ptr(&a_label, a_msg_s, a_msg_e, &a_depth);
		label_ptr(&b_label, b_msg_s, b_msg_e, &b_depth);

		if (!label_cmp(a_label, a_msg_e, b_label, b_msg_e)) {
			PERR("1:DNS:NAME_CMP:END:NEQ\n");
			return false;
		}

		label_sz = a_label[0];
		
		if (label_sz == 0) {
			PERR("1:DNS:NAME_CMP:END:EQ\n");
			return true;
		}

		a_label += label_sz + 1;
		b_label += label_sz + 1;
	}
}

static void name_dump(u8 *name, u8 *msg_s, u8 *msg_e)
{
	u8 *label;
	u8 name_depth;
	u8 label_idx;

	PERR("1:DNS:NAME_DUMP:START\n");
	PERR("1:DNS:NAME_DUMP:0x%p[0x%p-0x%p]\n", name, msg_s, msg_e - 1);

	label = name;
	name_depth = CONFIG_DNS_LABEL_PTR_DEPTH_MAX;
	label_idx = 0;
	loop {
		u8 *label_l;

		label_ptr(&label, msg_s, msg_e, &name_depth);

		if (label >= msg_e) {
			PERR("0:DNS:NAME_DUMP:ERROR:label[%u] is out of bounds\n", label_idx);
			exit(1);
		}

		if (label[0] == 0)
			break;

		label_l = label + 1 + label[0] - 1;
		if (label_l >= msg_e ) {
			PERR("0:DNS:NAME_DUMP:ERROR:label[%u] is going out of bounds\n", label_idx);
			exit(1);
		}
		PERR("1:DNS:NAME_DUMP:label[%u]=%.*s(%u bytes)\n", label_idx, label[0], label + 1, label[0]);

		label = label_l + 1;
		++label_idx;
	}
	PERR("1:DNS:NAME_DUMP:END\n");
}

static void *name_skip(u8 *name, u8 *msg_s, u8 *msg_e)
{
	u8 *label;

	PERR("1:DNS:NAME_SKIP:START\n");
	PERR("1:DNS:NAME_SKIP:0x%p[0x%p-0x%p]\n", name, msg_s, msg_e - 1);
	
	label = name;
	loop {
		PERR("DNS:NAME_SKIP:LABEL:0x%p\n", label);
		if (label >= msg_e) {
			PERR("0:DNS:NAME_SKIP:ERROR:label is out of bounds\n");
			exit(1);
		}

		if (label[0] == 0) {
			PERR("1:DNS:NAME_SKIP:END:ZERO\n");
			return label + 1;
		}

		if (IS_PTR(label[0])) {
			PERR("1:DNS:NAME_SKIP:END:POINTER\n");
			return label + 2;
		}
		
		label += label[0] + 1;
	}
}
#undef IS_PTR
/*----------------------------------------------------------------------------*/
#define CLEANUP
#include "namespace/ulinux.h"
#include "namespace/syncsm.h"
#include "dns/namespace/name.h"
#include "dns/namespace/name.c"
#undef CLEANUP
#endif
