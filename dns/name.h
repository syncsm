#ifndef SYNCSM_DNS_NAME_H
#define SYNCSM_DNS_NAME_H
#include "namespace/ulinux.h"
#include "namespace/syncsm.h"
#include "dns/namespace/name.h"
static u8 *name_build(u8 *name, struct str_slice_t *dn);
static bool name_cmp(u8 *a, u8 *a_msg_s, u8* a_msg_e, u8 *b, u8 *b_msg_s,
								u8* b_msg_e);
static u8 *name_cpy(u8 *d, u8 *s, u8 *s_msg_s, u8 *s_msg_e);
static void name_dump(u8 *name, u8 *msg_s, u8 *msg_e);
static void *name_skip(u8 *name, u8 *msg_s, u8 *msg_e);
#define CLEANUP
#include "namespace/ulinux.h"
#include "namespace/syncsm.h"
#include "dns/namespace/name.h"
#undef CLEANUP
#endif
