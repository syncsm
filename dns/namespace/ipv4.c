/*
 * this code is protected by the GNU affero GPLv3 license
 * author:Sylvain BERTRAND
 */
#ifndef CLEANUP
#define connect_ns        syncsm_dns_ipv4_c_connect_ns
#define ns_resp_wait      syncsm_dns_ipv4_c_ns_resp_wait
#define qtion_send_ns     syncsm_dns_ipv4_c_qtion_send_ns
#define sa                syncsm_dns_ipv4_c_sa
#define sa_init_ns        syncsm_dns_ipv4_c_sa_init_ns
#define timer_expired     syncsm_dns_ipv4_c_timer_expired
#define udp_datagram_read syncsm_dns_ipv4_c_udp_datagram_read
#define udp_so            syncsm_dns_ipv4_c_udp_so
/******************************************************************************/
#else /* CLEANUP */
#undef connect_ns
#undef ns_resp_wait
#undef qtion_send_ns
#undef sa
#undef sa_init_ns
#undef timer_expired
#undef udp_datagram_read
#undef udp_so
#endif
