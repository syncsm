/*
 * this code is protected by the GNU affero GPLv3 license
 * author:Sylvain BERTRAND
 */
#ifndef CLEANUP
#define ipv4_ns           syncsm_dns_ipv4_ns
#define ipv4_udp_so_setup syncsm_dns_ipv4_udp_so_setup 
/******************************************************************************/
#else /* CLEANUP */
#undef ipv4_ns 
#undef ipv4_udp_so_setup
#endif
