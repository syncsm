/*
 * this code is protected by the GNU affero GPLv3 license
 * author:Sylvain BERTRAND
 */
#ifndef CLEANUP
#define ipv6_ns           syncsm_dns_ipv6_ns
#define ipv6_udp_so_setup syncsm_dns_ipv6_udp_so_setup 
/******************************************************************************/
#else /* CLEANUP */
#undef ipv6_ns 
#undef ipv6_udp_so_setup
#endif
