/*
 * this code is protected by the GNU affero GPLv3 license
 * author:Sylvain BERTRAND
 */
#ifndef CLEANUP
#define label_cmp              syncsm_dns_name_c_label_cmp
#define label_cpy              syncsm_dns_name_c_label_cpy
#define label_ptr              syncsm_dns_name_c_label_ptr   
/******************************************************************************/
#else /* CLEANUP */
#undef label_cmp
#undef label_cpy
#undef label_ptr
#endif

