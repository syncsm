/*
 * this code is protected by the GNU affero GPLv3 license
 * author:Sylvain BERTRAND
 */
#ifndef CLEANUP
#define name_build      syncsm_dns_name_build
#define name_cmp      syncsm_dns_name_cmp
#define name_cpy      syncsm_dns_name_cpy
#define name_dump     syncsm_dns_name_dump
#define name_skip     syncsm_dns_name_skip
/******************************************************************************/
#else /* CLEANUP */
#undef name_build
#undef name_cmp
#undef name_cpy
#undef name_dump
#undef name_skip
#endif
