/*
 * this code is protected by the GNU affero GPLv3 license
 * author:Sylvain BERTRAND
 */
#ifndef CLEANUP
#define query_build syncsm_dns_query_build
/******************************************************************************/
#else /* CLEANUP */
#undef query_build
#endif
