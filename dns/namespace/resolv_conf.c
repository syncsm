/*
 * this code is protected by the GNU affero GPLv3 license
 * author:Sylvain BERTRAND
 */
#ifndef CLEANUP
#define parse_line             syncsm_dns_resol_conf_c_parse_line
#define resolv_conf            syncsm_dns_resol_conf_c_resolv_conf
#define resolv_conf_sz         syncsm_dns_resol_conf_c_resolv_conf_sz
/******************************************************************************/
#else /* CLEANUP */
#undef parse_line
#undef resolv_conf
#undef resolv_conf_sz
#endif
