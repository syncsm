/*
 * this code is protected by the GNU affero GPLv3 license
 * author:Sylvain BERTRAND
 */
#ifndef CLEANUP
#define resolv_conf_parse      syncsm_dns_resolv_conf_parse
#define resolv_conf_read       syncsm_dns_resolv_conf_read
/******************************************************************************/
#else /* CLEANUP */
#undef resolv_conf_parse
#undef resolv_conf_read
#endif
