/*
 * this code is protected by the GNU affero GPLv3 license
 * author:Sylvain BERTRAND
 */
#ifndef CLEANUP
#define canonical_dn_ips_collect          syncsm_dns_resolver_c_canonical_dn_ips_collect
#define canonical_dn_ips_collect_from_net syncsm_dns_resolver_c_canonical_dn_ips_collect_from_net
#ifdef CONFIG_SMTP_IPV4
#define canonical_dn_ipv4_collect_from_net syncsm_dns_resolver_c_canonical_dn_ipv4_collect_from_net
#endif
#define canonical_dn_ipv6_collect_from_net syncsm_dns_resolver_c_canonical_dn_ipv6_collect_from_net
#define dump_dn_ips                        syncsm_dns_resolver_c_dump_dn_ips
#define dump_mxs_pref                      syncsm_dns_resolver_c_dump_mxs_pref
#define mx_cmp                             syncsm_dns_resolver_c_mx_cmp
#define mx_ips_collect_from_net            syncsm_dns_resolver_c_mx_ips_collect_from_net
#ifdef CONFIG_SMTP_IPV4
#define mx_ipv4_collect_from_net           syncsm_dns_resolver_c_mx_ipv4_collect_from_net
#endif
#define mx_ipv6_collect_from_net           syncsm_dns_resolver_c_mx_ipv6_collect_from_net
#define mxs_ips_collect                    syncsm_dns_resolver_c_mxs_ips_collect
#define mxs_ips_collect_from_net           syncsm_dns_resolver_c_mxs_ips_collect_from_net
#define mxs_ips_collect_from_resp_ar       syncsm_dns_resolver_c_mxs_ips_collect_from_resp_ar
#define ns_ask                             syncsm_dns_resolver_c_ns_ask
#define ns_net_ask                         syncsm_dns_resolver_c_ns_net_ask
#define ns_timer_start                     syncsm_dns_resolver_c_ns_timer_start
#define nss_ask                            syncsm_dns_resolver_c_nss_ask
#define qtion_ask_ns                       syncsm_dns_resolver_c_qtion_ask_ns
#define resolver                           syncsm_dns_resolver_c_resolver
#define timer_setup                        syncsm_dns_resolver_c_timer_setup
#define udp_so_setup                       syncsm_dns_resolver_c_udp_so_setup
/******************************************************************************/
#else /* CLEANUP */
#undef canonical_dn_ips_collect
#undef canonical_dn_ips_collect_from_net
#ifdef CONFIG_SMTP_IPV4
#undef canonical_dn_ipv4_collect_from_net
#endif
#undef canonical_dn_ipv6_collect_from_net
#undef dump_dn_ips
#undef dump_mxs_pref
#undef mx_cmp
#undef mx_ips_collect_from_net
#ifdef CONFIG_SMTP_IPV4
#undef mx_ipv4_collect_from_net
#endif
#undef mx_ipv6_collect_from_net
#undef mxs_ips_collect
#undef mxs_ips_collect_from_net
#undef mxs_ips_collect_from_resp_ar
#undef ns_ask
#undef ns_net_ask
#undef ns_timer_start
#undef nss_ask
#undef qtion_ask_ns
#undef resolver
#undef timer_setup
#undef udp_so_setup
#endif
