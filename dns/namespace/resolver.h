/*
 * this code is protected by the GNU affero GPLv3 license
 * author:Sylvain BERTRAND
 */
#ifndef CLEANUP
#define epfd_v        syncsm_dns_resolver_epfd_v
#define perr_ns       syncsm_dns_resolver_perr_ns
#define qtion_dump    syncsm_dns_resolver_qtion_dump
#define rr_class_str  syncsm_dns_resolver_rr_class_str
#define rr_type_str   syncsm_dns_resolver_rr_type_str
#define timerfd_v     syncsm_dns_resolver_timerfd_v
/******************************************************************************/
#else /* CLEANUP */
#undef epfd_v
#undef perr_ns
#undef qtion_dump
#undef rr_class_str
#undef rr_type_str
#undef timerfd_v
#endif
