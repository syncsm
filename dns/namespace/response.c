/*
 * this code is protected by the GNU affero GPLv3 license
 * author:Sylvain BERTRAND
 */
#ifndef CLEANUP
#define addr_match_mx_exchange syncsm_dns_resp_c_addr_match_mx_exchange
#define addr_store_smtp_ip     syncsm_dns_resp_c_addr_store_smtp_ip
#define additionals_parse      syncsm_dns_resp_c_additionals_parse
#define answers_parse          syncsm_dns_resp_c_answers_parse
#define authorities_parse      syncsm_dns_resp_c_authorities_parse
#define basic_and_hdr_chks     syncsm_dns_resp_c_basic_and_hdr_chks
#define hdr_info_chk           syncsm_dns_resp_c_hdr_info_chk
#define is_addr                syncsm_dns_resp_c_is_addr
#define mx_store               syncsm_dns_resp_c_mx_store
#define qtion_chk              syncsm_dns_resp_c_qtion_chk
#define qtions_parse           syncsm_dns_resp_c_qtions_parse
#define rrs_parse              syncsm_dns_resp_c_rrs_parse
/******************************************************************************/
#else /* CLEANUP */
#undef addr_match_mx_exchange
#undef addr_store_smtp_ip
#undef additionals_parse
#undef answers_parse
#undef authorities_parse
#undef basic_and_hdr_chks
#undef hdr_info_chk
#undef is_addr
#undef mx_store
#undef qtion_chk
#undef qtions_parse
#undef rrs_parse
#endif
