/*
 * this code is protected by the GNU affero GPLv3 license
 * author:Sylvain BERTRAND
 */
#ifndef CLEANUP
#define resp_an_collect_mx_ips           syncsm_dns_resp_an_collect_mx_ips
#define resp_an_collect_canonical_dn_ips syncsm_dns_esp_an_collect_canonical_dn_ips
#define resp_ar_collect_mx_ips           syncsm_dns_resp_ar_collect_mx_ips
#define resp_ar_collect_canonical_dn_ips syncsm_dns_resp_ar_collect_canonical_dn_ips
#define resp_basic_parse                 syncsm_dns_resp_basic_parse
#define resp_cname_rr_scan               syncsm_dns_resp_cname_rr_scan
#define resp_mxs_scan                    syncsm_dns_resp_mxs_scan
/******************************************************************************/
#else /* CLEANUP */
#undef resp_an_collect_mx_ips
#undef resp_an_collect_canonical_dn_ips
#undef resp_ar_collect_mx_ips
#undef resp_ar_collect_canonical_dn_ips
#undef resp_basic_parse
#undef resp_cname_rr_scan
#undef resp_mxs_scan
#endif
