/*
 * this code is protected by the GNU affero GPLv3 license
 * author:Sylvain BERTRAND
 */
#ifndef CLEANUP
#define canonical_dn_v             syncsm_dns_state_canonical_dn_v
#define canonical_dn_e_v           syncsm_dns_state_canonical_dn_e_v
#define cname_rr_v                 syncsm_dns_state_cname_rr_v
#define dn_v                       syncsm_dns_state_dn_v
#define id_v                       syncsm_dns_state_id_v
#define mx_n                       syncsm_dns_state_mx_n
#define mxs_v                      syncsm_dns_state_mxs_v
#define mxs_n_v                    syncsm_dns_state_mxs_n_v
#define ns_v                       syncsm_dns_state_ns_v
#define nss_v                      syncsm_dns_state_nss_v
#define nss_n_v                    syncsm_dns_state_nss_n_v
#define qtion_dn_v                 syncsm_dns_state_qtion_dn_v
#define qtion_dn_e_v               syncsm_dns_state_qtion_dn_e_v
#define qtion_type_v               syncsm_dns_state_qtion_type_v
#define query_e_v                  syncsm_dns_state_query_e_v
#define query_qtion_e_v            syncsm_dns_state_query_qtion_e_v
#define query_qtion_s_v            syncsm_dns_state_query_qtion_s_v
#define query_qtion_sz_v           syncsm_dns_state_query_qtion_sz_v
#define query_qtion_trailer_v      syncsm_dns_state_query_qtion_trailer_v
#define query_sz_v                 syncsm_dns_state_query_sz_v
#define query_v                    syncsm_dns_state_query_v
#define resp_additionals_rrs_v     syncsm_dns_state_resp_additionals_rrs_v
#define resp_additionals_s_v       syncsm_dns_state_resp_additionals_s_v
#define resp_answers_rrs_v         syncsm_dns_state_resp_answers_rrs_v
#define resp_answers_s_v           syncsm_dns_state_resp_answers_s_v
#define resp_authorities_rrs_v     syncsm_dns_state_resp_authorities_rrs_v
#define resp_authorities_s_v       syncsm_dns_state_resp_authorities_s_v
#define resp_e_v                   syncsm_dns_state_resp_e_v
#define resp_qtion_t               syncsm_dns_state_resp_qtion_t
#define resp_qtion_v               syncsm_dns_state_resp_qtion_v
#define resp_qtions_s_v            syncsm_dns_state_resp_qtions_s_v
#define resp_sz_v                  syncsm_dns_state_resp_sz_v
#define resp_v                     syncsm_dns_state_resp_v
#define rr_t                       syncsm_dns_state_rr_t
#define rrs_n_max                  syncsm_dns_state_rrs_n_max
/******************************************************************************/
#else /* CLEANUP */
#undef canonical_dn_v
#undef canonical_dn_e_v
#undef cname_rr_v
#undef dn_v
#undef id_v
#undef mx_n
#undef mxs_v
#undef mxs_n_v
#undef ns_v
#undef nss_v
#undef nss_n_v
#undef qtion_dn_v
#undef qtion_dn_e_v
#undef qtion_type_v
#undef query_e_v
#undef query_qtion_e_v
#undef query_qtion_s_v
#undef query_qtion_sz_v
#undef query_qtion_trailer_v
#undef query_sz_v
#undef query_v
#undef resp_additionals_rrs_v
#undef resp_additionals_s_v
#undef resp_answers_rrs_v
#undef resp_answers_s_v
#undef resp_authorities_rrs_v
#undef resp_authorities_s_v
#undef resp_e_v
#undef resp_qtion_t
#undef resp_qtion_v
#undef resp_qtions_s_v
#undef resp_sz_v
#undef resp_v
#undef rr_t
#undef rrs_n_max
#endif
