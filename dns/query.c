#ifndef SYNCSM_DNS_QUERY_C
#define SYNCSM_DNS_QUERY_C
/*
 * this code is protected by the GNU affero GPLv3 license
 * author:Sylvain BERTRAND
 */
/*----------------------------------------------------------------------------*/
/* compiler stuff */
#include <stdbool.h>
#include <stdarg.h>
/*----------------------------------------------------------------------------*/
#include "config.h"
/*----------------------------------------------------------------------------*/
#include "ulinux.h"
/*----------------------------------------------------------------------------*/
#include "dns/rfc.h"
#include "smtp/rfc.h"
/*----------------------------------------------------------------------------*/
#include "perr.h"
#include "syncsm.h"
/*----------------------------------------------------------------------------*/
#include "dns/state.h"
#include "dns/resolver.h"
/*============================================================================*/
#include "namespace/ulinux.h"
#include "dns/namespace/resolver.h"
#include "dns/namespace/state.h"
#include "dns/namespace/query.h"
/*----------------------------------------------------------------------------*/
/* exported in query.h */
static void query_build(void)
{
	struct dns_rfc_hdr_t *hdr;
	u8 *name;
	u8 name_sz;
	/*-------------------------------------------------------------------*/
	hdr = (struct dns_rfc_hdr_t*)query_v;

	memset(hdr, 0, sizeof(*hdr));
	hdr->id = id_v; /* we don't care about the endian */
	++id_v;

	/*
	 * many dns servers out there supports _only_ recursive queries,
	 * which should have been the default and only choice since the
	 * beginning for clients
	 */
	hdr->info |= cpu_to_be16(DNS_RFC_HDR_INFO_RD_BIT);

	hdr->qdcount = cpu_to_be16(1); /* one mx question */
	/*-------------------------------------------------------------------*/
	/* right after the header is our question  */
	query_qtion_s_v = &query_v[sizeof(*hdr)];
	name = query_qtion_s_v;

	name_sz = (u8)(qtion_dn_e_v - qtion_dn_v);

	memcpy(name, qtion_dn_v, name_sz);
	/*-------------------------------------------------------------------*/
	query_qtion_trailer_v = (struct dns_rfc_qtion_trailer_t*)(name
								+ name_sz);
	query_qtion_trailer_v->type = cpu_to_be16(qtion_type_v);
	query_qtion_trailer_v->class = cpu_to_be16(DNS_RFC_RR_CLASS_IN);
	/*-------------------------------------------------------------------*/
	query_qtion_e_v = (u8*)query_qtion_trailer_v
					+ sizeof(*query_qtion_trailer_v);
	query_qtion_sz_v = (u16)(query_qtion_e_v - query_qtion_s_v);

	query_e_v = query_qtion_e_v; /* 1 question, then same size */
	query_sz_v = (u16)(query_qtion_e_v - query_v);

	PERR("1:DNS:built question follows:\n");
	qtion_dump(query_qtion_s_v, query_v, query_e_v);
	PERR("1:DNS:QUERY:[0x%p-0x%p]\n", query_v, query_e_v - 1);
}
/*----------------------------------------------------------------------------*/
#define CLEANUP
#include "namespace/ulinux.h"
#include "dns/namespace/resolver.h"
#include "dns/namespace/state.h"
#include "dns/namespace/query.h"
#undef CLEANUP
#endif
