#ifndef SYNCSM_DNS_QUERY_H
#define SYNCSM_DNS_QUERY_H
#include "dns/namespace/query.h"
/*
 * this code is protected by the GNU affero GPLv3 license
 * author:Sylvain BERTRAND
 */
static void query_build(void);
#define CLEANUP
#include "dns/namespace/query.h"
#undef CLEANUP
#endif
