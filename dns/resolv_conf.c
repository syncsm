#ifndef SYNCSM_DNS_RESOLV_CONF_C
#define SYNCSM_DNS_RESOLV_CONF_C
#include <stdbool.h>
#include <stdarg.h>
/*----------------------------------------------------------------------------*/
#include "config.h"
/*----------------------------------------------------------------------------*/
#include "ulinux.h"
/*----------------------------------------------------------------------------*/
#include "dns/rfc.h"
#include "smtp/rfc.h"
/*----------------------------------------------------------------------------*/
#include "perr.h"
#include "syncsm.h"
#include "dns/state.h"
#include "dns/resolver.h"
/*============================================================================*/
#include "namespace/ulinux.h"
#include "namespace/syncsm.h"
#include "dns/namespace/state.h"
#include "dns/namespace/resolver.h"
#include "dns/namespace/resolv_conf.h"
#include "dns/namespace/resolv_conf.c"
/*----------------------------------------------------------------------------*/
static u8 resolv_conf[CONFIG_BUFSIZ];
static u64 resolv_conf_sz;
/*----------------------------------------------------------------------------*/
/*
 * on entry, e point either on the byte paste the last resolve.conf byte
 * or the end of line mark, namely '\n'.
 * in other words, e points past one byte the last valid line char.
 */
static void parse_line(u8 line, u8 *s, u8 *e)
{
	u8 *c;
	u8 *ipvx_s; /* first ipvx char */
	u8 *ipvx_l; /* last ipvx char */

	c = s;
	/* locate first non-space char */
	loop {
		if (c == e)
			return;

		if (!is_space(*c)) /* does include '\n' */
			break;
		++c;
	}

	if (*c == '#' || *c == ';')  /* this is a comment line */
		return;

	if ((e - c) < CSTRLEN("nameserver")) {
		/* no room for a nameserver keyword */
		return;
	}

	if (!memcmp(c, "nameserver", CSTRLEN("nameserver"))) {
		/* not a nameserver keyword */
		return;
	}

	/* we have a nameserver keyword, skip spaces */
	c += CSTRLEN("nameserver");
	loop {
		if (c == e) {
			PERR("0:DNS:RESOLV_CONF:ERROR:\"%s\":line %d:missing ip address of nameserver keyword\n", CONFIG_RESOLV_CONF_PATH, line);
			exit(1);
		}

		if (!is_space(*c))
			break;
		++c;
	}

	/* then we should have a ipvx address */
	ipvx_s = c;

	/* locate the end of the ipvx address */
	loop {
		if (c == e || is_space(*c))
			break;
		++c;
	}
	ipvx_l = c - 1;

	/* oops ! */
	if (nss_n_v == CONFIG_DNS_NSS_N_MAX) {
		PERR("0:DNS:RESOLV_CONF:ERROR:\"%s\":no more room for dns servers (max is %u)\n", CONFIG_RESOLV_CONF_PATH, CONFIG_DNS_NSS_N_MAX);
		exit(1);
	}

#ifdef CONFIG_DNS_IPV4
	if (to_ipv4_blk(&nss_v[nss_n_v].ipv4_net, ipvx_s, ipvx_l)) {
		nss_v[nss_n_v].type = ipv4;
		++nss_n_v;
		return;
	}
#endif
	if (to_ipv6_blk(nss_v[nss_n_v].ipv6_net, ipvx_s, ipvx_l)) {
#if defined CONFIG_SMTP_IPV4 || defined CONFIG_DNS_IPV4
		nss_v[nss_n_v].type = ipv6;
#endif
		++nss_n_v;
		return;
	} 
#ifdef CONFIG_DNS_IPV4
	PERR("0:DNS:RESOLV_CONF:\"%s\":line %d:ip address is not v4 or v6:\"%.*s\"\n", CONFIG_RESOLV_CONF_PATH, line, ipvx_l - ipvx_s + 1, ipvx_s);
#endif
	PERR("0:DNS:RESOLV_CONF:\"%s\":line %d:ip address is not ipv6:\"%.*s\"\n", CONFIG_RESOLV_CONF_PATH, line, ipvx_l - ipvx_s + 1, ipvx_s);
}
/******************************************************************************/
/* exported in resolv_conf.h */
static void resolv_conf_parse(void)
{
	u8 line;
	u8 *line_s;
	u8 *line_e;
	u8 *file_e;
	u8 i;

	nss_n_v = 0;
	if (resolv_conf_sz == 0) {
		PERR("0:DNS:RESOLV_CONF:\"%s\" is empty, nothing to parse\n", CONFIG_RESOLV_CONF_PATH);
		return;
	}
	/*--------------------------------------------------------------------*/
	/* parse lines */
	file_e = resolv_conf + resolv_conf_sz;
	line_s = resolv_conf;
	line_e = resolv_conf;

	line = 1;	
	loop { 
		/* locate the end of the line */
		loop {
			if (line_e == file_e || *line_e == '\n')
				break;
			++line_e;
		};

		parse_line(line, line_s, line_e);

		if (line_e == file_e)
			break;

		line_s = line_e + 1;
		line_e = line_s;
		++line;
	}
	/*--------------------------------------------------------------------*/
	/* tell the user */
	PERR("0:DNS:RESOLV_CONF:\"%s\" configured %d name server(s)\n", CONFIG_RESOLV_CONF_PATH, nss_n_v);
	i = 0;
	loop {
		if (i == nss_n_v)
			break;
		perr_ns(0, "DNS:RESOLV_CONF", i);
		++i;
	}
}

static void resolv_conf_read(void)
{
	sl r;
	si fd; 
	struct stat s;

	r = openat(AT_CWD, CONFIG_RESOLV_CONF_PATH, O_NONBLOCK, O_RD_ONLY);
	if (ISERR(r)) {
		PERR("0:DNS:RESOLV_CONF:ERROR:unable to open resolv.conf file \"%s\"\n", CONFIG_RESOLV_CONF_PATH);
		exit(1);
	}

	fd = (si)r;

	r = fstat(fd, &s);
	if (s.size > CONFIG_BUFSIZ) {
		PERR("0:DNS:RESOLV_CONF:ERROR:resolv.conf is %ld bytes and cannot fit into the buffer of %d bytes\n", s.size, CONFIG_BUFSIZ); 
		exit(1);
	}

	resolv_conf_sz = 0;
	loop {
		r = read(fd, resolv_conf + resolv_conf_sz, s.size);
		if (ISERR(r)) {
			if (r == -EAGAIN || r == -EINTR)
				continue;
			PERR("0:DNS:RESOLV_CONF:ERROR:%ld:reading \"%s\"\n", r, CONFIG_RESOLV_CONF_PATH);
			exit(1);
		}
		if (r == 0) /* end of file */
			break;
		resolv_conf_sz += (u64)r;
	}	
	PERR("0:DNS:RESOLV_CONF:read %ld bytes from \"%s\"\n", resolv_conf_sz, CONFIG_RESOLV_CONF_PATH);
	close(fd);
}
/*----------------------------------------------------------------------------*/
#define CLEANUP
#include "namespace/ulinux.h"
#include "namespace/syncsm.h"
#include "dns/namespace/state.h"
#include "dns/namespace/resolver.h"
#include "dns/namespace/resolv_conf.h"
#undef CLEANUP
#endif
