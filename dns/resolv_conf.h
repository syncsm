#ifndef SYNCSM_DNS_RESOLV_CONF_H
#define SYNCSM_DNS_RESOLV_CONF_H
#include "dns/namespace/resolv_conf.h"
/*
 * this code is protected by the GNU affero GPLv3 license
 * author:Sylvain BERTRAND
 */
static void resolv_conf_parse(void);
static void resolv_conf_read(void);
#define CLEANUP
#include "dns/namespace/resolv_conf.h"
#undef CLEANUP
#endif
