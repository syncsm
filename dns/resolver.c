#ifndef SYNCSM_DNS_RESOLVER_C
#define SYNCSM_DNS_RESOLVER_C
/*
 * this code is protected by the GNU affero GPLv3 license
 * author:Sylvain BERTRAND
 */
/* compiler stuff */
#include <stdbool.h>
#include <stdarg.h>
/*----------------------------------------------------------------------------*/
#include "config.h"
/*----------------------------------------------------------------------------*/
#include "ulinux.h"
/*----------------------------------------------------------------------------*/
#include "dns/rfc.h"
#include "smtp/rfc.h"
/*----------------------------------------------------------------------------*/
#include "perr.h"
#include "syncsm.h"
/*----------------------------------------------------------------------------*/
#include "dns/state.h"
#include "dns/resolver.h"
#include "dns/ipv4.h"
#include "dns/ipv6.h"
#include "dns/query.h"
#include "dns/response.h"
#include "dns/name.h"
#include "dns/resolv_conf.h"
/*============================================================================*/
#include "namespace/ulinux.h"
#include "namespace/syncsm.h"
#include "namespace/dns.h"
#include "dns/namespace/state.h"
#include "dns/namespace/resolver.h"
#include "dns/namespace/ipv4.h"
#include "dns/namespace/ipv6.h"
#include "dns/namespace/query.h"
#include "dns/namespace/response.h"
#include "dns/namespace/name.h"
#include "dns/namespace/resolv_conf.h"
#include "dns/namespace/resolver.c"
/*----------------------------------------------------------------------------*/
static void udp_so_setup(void)
{
#ifdef CONFIG_DNS_IPV4
	ipv4_udp_so_setup();
#endif
	ipv6_udp_so_setup();
}

static void timer_setup(void)
{
	sl r;
	struct epoll_event evts;

	r = timerfd_create(CLOCK_MONOTONIC, TFD_NONBLOCK);
	if (ISERR(r)) {
		PERR("0:DNS:ERROR:%ld:unable to create the timer file descriptor\n", r);
		exit(1);
	}
	timerfd_v = (si)r;

	/* add the timerfd to the epollfd */
	memset(&evts, 0, sizeof(evts));
	/* could be EPOLLET since we deal only with 1 expiration */
	evts.events = EPOLLIN;
	evts.data.fd = timerfd_v;
	r = epoll_ctl(epfd_v, EPOLL_CTL_ADD, timerfd_v, &evts);
	if (ISERR(r)) {
		PERR("0:DNS:ERROR:%ld:unable to add the timer file descriptor to the epoll file descriptor\n", r);
		exit(1);
	}
}

static void ns_timer_start(void)
{
	sl r;
	struct itimerspec itimerspec;

	memset(&itimerspec, 0, sizeof(itimerspec));

	/* initial expiration in the futur of current monotonic clock */
	itimerspec.value.sec = CONFIG_DNS_NS_RESP_WAIT_TIME_SEC;
	itimerspec.value.nsec = CONFIG_DNS_NS_RESP_WAIT_TIME_NSEC;

	/* relative initial expiration */
	r = timerfd_settime(timerfd_v, 0, &itimerspec, 0);
	if (ISERR(r)) {
		PERR("0:DNS:ERROR:%ld:unable to arm the name server response timer for %lu sec and %lu nanosec\n", r, CONFIG_DNS_NS_RESP_WAIT_TIME_SEC, CONFIG_DNS_NS_RESP_WAIT_TIME_NSEC);
		exit(1);
	}
}

/******************************************************************************/
/* exported in resolver.h */
static void perr_ns(u8 lvl, u8 *hdr_str, u8 i)
{
	PERR("%u:%s:NAME_SERVER[%d]:", lvl, hdr_str, i);
#ifdef CONFIG_DNS_IPV4
		switch (nss_v[i].type) {
		case ipv4:
			PERR("IPV4:0x%08x\n", be32_to_cpu(nss_v[i].ipv4_net));
			break;
		case ipv6:
#endif
			PERR("IPV6:0x%016lx%016lx\n", be64_to_cpu(nss_v[i].ipv6_net_h), be64_to_cpu(nss_v[i].ipv6_net_l));
#ifdef CONFIG_DNS_IPV4
			break;
		}
#endif
}

static u8 *rr_type_str(u16 type)
{
	switch (type) {
	case DNS_RFC_RR_TYPE_A:
		return "A";
	case DNS_RFC_RR_TYPE_NS:
		return "NS";
	case DNS_RFC_RR_TYPE_MD:
		return "MD";
	case DNS_RFC_RR_TYPE_MF:
		return "MF";
	case DNS_RFC_RR_TYPE_CNAME:
		return "CNAME";
	case DNS_RFC_RR_TYPE_SOA:
		return "SOA";
	case DNS_RFC_RR_TYPE_MB:
		return "MB";
	case DNS_RFC_RR_TYPE_MG:
		return "MG";
	case DNS_RFC_RR_TYPE_MR:
		return "MR";
	case DNS_RFC_RR_TYPE_NULL:
		return "NULL";
	case DNS_RFC_RR_TYPE_WKS:
		return "WKS";
	case DNS_RFC_RR_TYPE_PTR:
		return "PTR";
	case DNS_RFC_RR_TYPE_HINFO:
		return "HINFO";
	case DNS_RFC_RR_TYPE_MINFO:
		return "MINFO";
	case DNS_RFC_RR_TYPE_MX:
		return "MX";
	case DNS_RFC_RR_TYPE_TXT:
		return "TXT";
	case DNS_RFC_RR_TYPE_AAAA:
		return "AAAA";
	default:
		return "UNKNOWN";
	}
}

static u8 *rr_class_str(u16 class)
{
	switch (class) {
	case DNS_RFC_RR_CLASS_IN:
		return "IN";
	case DNS_RFC_RR_CLASS_CN:
		return "CN";
	case DNS_RFC_RR_CLASS_CH:
		return "CH";
	case DNS_RFC_RR_CLASS_HS:
		return "HS";
	default:
		return "UNKNOWN";
	}
}

static bool qtion_dump(u8 *qtion, u8 *msg_s, u8 *msg_e)
{
	struct dns_rfc_qtion_trailer_t *trailer;
	u8* trailer_l;

	PERR("1:DNS:QUESTION_DUMP:START:0x%p\n", qtion);

	name_dump(qtion, msg_s, msg_e);

	trailer = name_skip(qtion, msg_s, msg_e);

	trailer_l = (u8*)trailer + sizeof(*trailer) - 1;
	if (trailer_l >= msg_e) {
		PERR("0:DNS:QUESTION_DUMP:ERROR:question trailer is going out of bounds\n");
		return false;
	}
	PERR("1:DNS:QUESTION_DUMP:type=%s(0x%04x)\n", rr_type_str(be16_to_cpu(trailer->type)), be16_to_cpu(trailer->type));
	PERR("1:DNS:QUESTION_DUMP:class=%s(0x%04x)\n", rr_class_str(be16_to_cpu(trailer->class)), be16_to_cpu(trailer->class));
	PERR("1:DNS:QUESTION_DUMP:END\n");
	return true;
}

static bool ns_net_ask(void)
{
	sl r;

	ns_timer_start();

#ifdef CONFIG_DNS_IPV4
	if (nss_v[ns_v].type == ipv4)
		return ipv4_ns();
#endif
	return ipv6_ns();
}

static bool ns_ask(void)
{
	bool r;

	r = true;

	PERR("1:DNS:********************************************************************************\n");
	perr_ns(1, "DNS:RESOLVER", ns_v);
	if (!ns_net_ask()) {
		r = false;
		goto exit;
	}

	if (!resp_basic_parse()) {
		r = false;
		goto exit;
	}
exit:
	PERR("1:DNS:********************************************************************************\n");
	return r;
}

static void nss_ask(void)
{
	ns_v = 0;
	loop {
		if (ns_v == nss_n_v) {
			PERR("0:DNS:ERROR:\"%.*s\":no name server was able to give us a proper basic response for the current query\n", dn_v->dn.l - dn_v->dn.s + 1, dn_v->dn.s);
			exit(1);
		}
		
		if (ns_ask()) 
			break; /* got a basic response */
		++ns_v;
	}
}

#ifdef CONFIG_SMTP_IPV4
/* XXX: only if no ips were provided in the initial response */
static void mx_ipv4_collect_from_net(struct mx_t *mx)
{
	PERR("1:DNS:MX:A query on:\n");
	name_dump(mx->exchange, mx->exchange, mx->exchange
						+ DNS_RFC_NAME_SZ_MAX);
	qtion_type_v = DNS_RFC_RR_TYPE_A;
	query_build();

	nss_ask();

	resp_an_collect_mx_ips(mx);
}
#endif

/* XXX: only if no ips were provided in the initial response */
static void mx_ipv6_collect_from_net(struct mx_t *mx)
{
	PERR("1:DNS:MX:AAAA query on:\n");
	name_dump(mx->exchange, mx->exchange, mx->exchange
						+ DNS_RFC_NAME_SZ_MAX);
	qtion_type_v = DNS_RFC_RR_TYPE_AAAA;
	query_build();

	nss_ask();

	resp_an_collect_mx_ips(mx);
}

/* XXX: only if no ips were provided in the initial response */
static void mx_ips_collect_from_net(struct mx_t *mx)
{
	/* prepare the question name */
	qtion_dn_e_v = name_cpy(qtion_dn_v, mx->exchange,
			mx->exchange, mx->exchange + DNS_RFC_NAME_SZ_MAX);
#ifdef CONFIG_SMTP_IPV4
	mx_ipv4_collect_from_net(mx);
#endif
	mx_ipv6_collect_from_net(mx);
}

/* XXX: only if no ips were provided in the initial response */
static void mxs_ips_collect_from_net(void)
{
	u16 i;
	i = 0;
	loop {
		struct mx_t *mx;

		if (i == mxs_n_v)
			break;

		mx = &mxs_v[i];

		mx_ips_collect_from_net(mx);

		++i;
	};
}

static void mxs_ips_collect_from_resp_ar(void)
{
	u16 i;
	i = 0;
	loop {
		struct mx_t *mx;

		if (i == mxs_n_v)
			break;

		mx = &mxs_v[i];

		resp_ar_collect_mx_ips(mx);
		
		++i;
	};
}

static void mxs_ips_collect(void)
{
	mxs_ips_collect_from_resp_ar();
	if (dn_v->smtp_ips_n == 0)
		mxs_ips_collect_from_net();
}

static s8 mx_cmp(void *a, void *b)
{
	struct mx_t *a_mx;
	struct mx_t *b_mx;

	a_mx = a;
	b_mx = b;
	if (a_mx->preference < b_mx->preference)
		return QSORT_CMP_LT;
	else if (a_mx->preference == b_mx->preference)
		return QSORT_CMP_EQ;
	else if (a_mx->preference > b_mx->preference)
		return QSORT_CMP_GT;
}

#ifdef CONFIG_SMTP_IPV4
static void canonical_dn_ipv4_collect_from_net(void)
{
	PERR("1:DNS:CANONICAL_DOMAIN_NAME:A query on:\n");
	qtion_type_v = DNS_RFC_RR_TYPE_A;
	query_build();

	nss_ask();

	resp_an_collect_canonical_dn_ips();
}
#endif

static void canonical_dn_ipv6_collect_from_net(void)
{
	PERR("1:DNS:CANONICAL_DOMAIN_NAME:AAAA query on:\n");
	qtion_type_v = DNS_RFC_RR_TYPE_AAAA;
	query_build();

	nss_ask();

	resp_an_collect_canonical_dn_ips();
}

static void canonical_dn_ips_collect_from_net(void)
{
	/* prepare the question name */
	qtion_dn_e_v = name_cpy(qtion_dn_v, canonical_dn_v, canonical_dn_v,
							canonical_dn_e_v);
#ifdef CONFIG_SMTP_IPV4
	canonical_dn_ipv4_collect_from_net();
#endif
	canonical_dn_ipv6_collect_from_net();
}

static void canonical_dn_ips_collect(void)
{
	resp_ar_collect_canonical_dn_ips();

	if (dn_v->smtp_ips_n == 0)
		canonical_dn_ips_collect_from_net();
}

static void dump_dn_ips(void)
{
	u8 i;

	PERR("0:DNS:****************************************************************************\n");
	PERR("0:DNS:\"%.*s\":list of smtp ips\n", dn_v->dn.l - dn_v->dn.s + 1, dn_v->dn.s);

	i = 0;
	loop {
		struct ip_t *ip;

		if (i == dn_v->smtp_ips_n)
			break;

		PERR("DNS:----------------------------------------------------------------------------\n");

		ip = &dn_v->smtp_ips[i];	

#ifdef CONFIG_SMTP_IPV4
		if (ip->type == ipv4) {
			PERR("0:DNS:IPV4:0x%08lx\n", be32_to_cpu(ip->ipv4_net));
		} else {
#endif
			PERR("0:DNS:IPV6:0x%016lx%016lx\n", be64_to_cpu(ip->ipv6_net_h), be64_to_cpu(ip->ipv6_net_l));
#ifdef CONFIG_SMTP_IPV4
		}
#endif
			
		++i;
	}
	
	PERR("0:DNS:****************************************************************************\n");
}

static void dump_mxs_pref(void)
{
	u8 i;

	PERR("1:DNS:****************************************************************************\n");
	PERR("1:DNS:\"%.*s\":preference of mxs\n", dn_v->dn.l - dn_v->dn.s + 1, dn_v->dn.s);
	loop {
		if (i == mxs_n_v)
			break;
		PERR("1:DNS:\"%.*s\":mx[%u]:%u\n", dn_v->dn.l - dn_v->dn.s + 1, dn_v->dn.s, i, mxs_v[i].preference);
		++i;
	}
	PERR("1:DNS:****************************************************************************\n");
}


/* per domain name */
static void resolver(void)
{
	u8 *cname;
	/*
	 * high level resolver strategy.
	 * READ DNS.TXT IN THIS DIRECTORY.
	 * we diverge from dns.txt since we go query mx exchange a/aaaa only
	 * if, globally, we do not have already smtp ips.
	 */
	PERR("DNS:\"%.*s\":MX query\n", dn_v->dn.l - dn_v->dn.s + 1, dn_v->dn.s);
	qtion_dn_e_v = name_build(qtion_dn_v, &dn_v->dn);
	qtion_type_v = DNS_RFC_RR_TYPE_MX;
	query_build();

	nss_ask();

	/* select the proper canonical domain name */
	if(resp_cname_rr_scan()) {
		PERR("1:DNS:CANONICAL_NAME:mail domain name was an alias, provided canonical name follows:\n");
		cname = cname_rr_v->part->rdata; /* alias case */
	} else {
		PERR("1:DNS:CANONICAL_NAME:question mail domain name is the canonical name, which is:\n");
		cname = resp_qtion_v.name; /* direct case */
	}
	canonical_dn_e_v = name_cpy(canonical_dn_v, cname, resp_v,
							resp_e_v);
	name_dump(canonical_dn_v, canonical_dn_v, canonical_dn_v
						+ DNS_RFC_NAME_SZ_MAX);

	resp_mxs_scan();
	qsort(mxs_v, mxs_n_v, sizeof(struct mx_t), mx_cmp);
	dump_mxs_pref();

	/* time to collect some smtp ips */

	dn_v->smtp_ips_n = 0;

	if (mxs_n_v != 0)
		mxs_ips_collect();
	else {
		canonical_dn_ips_collect();
		if (dn_v->smtp_ips_n == 0) {
			PERR("0:DNS:\"%.*s\":ERROR:unable to get smtp ips\n", dn_v->dn.l - dn_v->dn.s + 1, dn_v->dn.s);
			exit(1);
		}
	}

	dump_dn_ips();
}

/******************************************************************************/
/* exported in dns.h */
/* we perform all dns resolution there, all domains before sending any email */
static void dns_resolver(void)
{
	u64 i = 0;
	loop {
		struct dn_al_t *d;

		if (i == dn_al_n_v)
			break;

		d = (struct dn_al_t*)&dn_al_v[i];

		if (d->type == domain_name) {
			PERR("1:DNS:RESOLVER:\"%.*s\"\n", d->dn.l - d->dn.s + 1, d->dn.s);

			dn_v = d; /* set the targetted domain name */
			resolver();
		}
		++i;
	}
}

static void dns_init(void)
{
	sl r;

	resolv_conf_read();
	resolv_conf_parse();

	r = epoll_create1(0);
	if (ISERR(r)) {
		PERR("0:DNS:ERROR:%ld:unable to create the epoll file descriptor\n", r);
		exit(1);
	}
	epfd_v = (si)r;

	timer_setup();
	udp_so_setup();

	/* our id counter */
	id_v = 0;
}
/*----------------------------------------------------------------------------*/
#define CLEANUP
#include "namespace/ulinux.h"
#include "namespace/syncsm.h"
#include "namespace/dns.h"
#include "dns/namespace/state.h"
#include "dns/namespace/resolver.h"
#include "dns/namespace/ipv4.h"
#include "dns/namespace/ipv6.h"
#include "dns/namespace/query.h"
#include "dns/namespace/response.h"
#include "dns/namespace/name.h"
#include "dns/namespace/resolv_conf.h"
#include "dns/namespace/resolver.c"
#undef CLEANUP
#endif
