#ifndef SYNCSM_DNS_RESOLVER_H
#define SYNCSM_DNS_RESOLVER_H
#include "namespace/ulinux.h"
#include "dns/namespace/resolver.h"
/*
 * this code is protected by the GNU affero GPLv3 license
 * author:Sylvain BERTRAND
 */
static si epfd_v;
static si timerfd_v;
static bool qtion_dump(u8 *question, u8 *msg_s, u8 *msg_e);
static u8 *rr_class_str(u16 class);
static u8 *rr_type_str(u16 type);
static void perr_ns(u8 lvl, u8 *hdr_str, u8 i);
#define CLEANUP
#include "namespace/ulinux.h"
#include "dns/namespace/resolver.h"
#undef CLEANUP
#endif
