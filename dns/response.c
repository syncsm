#ifndef SYNCSM_DNS_RESPONSE_C
#define SYNCSM_DNS_RESPONSE_C
/*
 * this code is protected by the GNU affero GPLv3 license
 * author:Sylvain BERTRAND
 */
/*
 * STRATEGY: once a severely malformed dns response is detected, the program
 * will exit.
 * do bound checking only right before memory access, because it is expensive.
 */
/*----------------------------------------------------------------------------*/
/* compiler stuff */
#include <stdbool.h>
#include <stdarg.h>
/*----------------------------------------------------------------------------*/
#include "config.h"
/*----------------------------------------------------------------------------*/
#include "ulinux.h"
/*----------------------------------------------------------------------------*/
#include "dns/rfc.h"
#include "smtp/rfc.h"
/*----------------------------------------------------------------------------*/
#include "perr.h"
#include "syncsm.h"
/*----------------------------------------------------------------------------*/
#include "dns/state.h"
#include "dns/resolver.h"
#include "dns/name.h"
/*============================================================================*/
#include "namespace/ulinux.h"
#include "namespace/syncsm.h"
#include "dns/namespace/state.h"
#include "dns/namespace/resolver.h"
#include "dns/namespace/name.h"
#include "dns/namespace/response.h"
#include "dns/namespace/response.c"
/*----------------------------------------------------------------------------*/
/* will store the pointer on the byte right after the last byte */
static bool rrs_parse(u8 **e, struct rr_t *rrs , u8 *s, u16 n)
{
	u8 *c;
	u16 i;

	c = s;
	i = 0;
	loop {
		u16 cpu_rdlength;

		if (i == n)
			break;
	
		PERR("1:DNS:RESPONSE:RR[%u]:START\n", i);

		rrs->name = c;
		name_dump(c, resp_v, resp_e_v);

		rrs->part = name_skip(c, resp_v, resp_e_v);
		PERR("1:DNS:RESPONSE:RR[%u]:FIELDS:0x%p\n", i, rrs->part);

		c = (u8*)rrs->part + sizeof(*(rrs->part));

		if (c >= resp_e_v) {
			PERR("1:DNS:RESPONSE:RR[%u]:WARNING:middle fields goes out of bounds, trying next name server\n", i);
			return false;
		}
		PERR("1:DNS:RESPONSE:RR[%u]:FIELDS:TYPE:%s\n", i, rr_type_str(be16_to_cpu(rrs->part->type)));
		PERR("1:DNS:RESPONSE:RR[%u]:FIELDS:CLASS:%s\n", i, rr_class_str(be16_to_cpu(rrs->part->class)));
		PERR("1:DNS:RESPONSE:RR[%u]:FIELDS:TTL:%d\n", i, be32_to_cpu(rrs->part->ttl));
		cpu_rdlength = be16_to_cpu(rrs->part->rdlength);
		PERR("1:DNS:RESPONSE:RR[%u]:FIELDS:RDLENGTH:0x%x\n", i, cpu_rdlength);

		c += cpu_rdlength;
		PERR("1:DNS:RESPONSE:RR[%u]:END\n", i);
		++rrs;
		++i;
	}
	*e = c;
	return true;
}

static bool additionals_parse(void)
{
	struct dns_rfc_hdr_t *hdr;
	u16 cpu_arcount;
	u8 *additionals_e;
	bool r;

	r = true;
	hdr = (struct dns_rfc_hdr_t*)resp_v;
	cpu_arcount = be16_to_cpu(hdr->arcount);

	PERR("1:DNS:RESPONSE:ADDITIONAL_SECTION:START\n");

	if (cpu_arcount > rrs_n_max) {
		PERR("0:DNS:RESPONSE:ADDITIONAL_SECTION:ERROR:too many additional resource record(s) %u, max is %u\n", cpu_arcount, rrs_n_max);
		exit(1);
	}
	PERR("1:DNS:RESPONSE:ADDITIONAL_SECTION:parsing %u resource records\n", cpu_arcount);

	if (rrs_parse(&additionals_e, resp_additionals_rrs_v,
					resp_additionals_s_v, cpu_arcount)) {
		if (additionals_e != resp_e_v)
			PERR("1:DNS:RESPONSE:ADDITIONAL_SECTION:WARNING:the end of section of additional resource record(s), 0x%p, is not the end of the udp datagram, 0x%p\n", additionals_e, resp_e_v);
	} else {
		r = false;
	}
	PERR("1:DNS:RESPONSE:ADDITIONAL_SECTION:END\n");
	return r;
}

static bool authorities_parse(void)
{
	struct dns_rfc_hdr_t *hdr;
	u16 cpu_nscount;
	bool r;

	hdr = (struct dns_rfc_hdr_t*)resp_v;
	cpu_nscount = be16_to_cpu(hdr->nscount);

	PERR("1:DNS:RESPONSE:AUTHORITY_SECTION:START\n");

	if (cpu_nscount > rrs_n_max) {
		PERR("0:DNS:RESPONSE:AUTHORITY_SECTION:ERROR:too many authority resource record(s) %u, max is %u\n", cpu_nscount, rrs_n_max);
		exit(1);
	}
	PERR("1:DNS:RESPONSE:AUTHORITY_SECTION:parsing %u resource records\n", cpu_nscount);

	r = rrs_parse(&resp_additionals_s_v, resp_authorities_rrs_v,
					resp_authorities_s_v, cpu_nscount);
	PERR("1:DNS:RESPONSE:AUTHORITY_SECTION:END\n");
	return r;
}

static bool answers_parse(void)
{
	struct dns_rfc_hdr_t *hdr;
	u16 cpu_ancount;
	bool r;

	hdr = (struct dns_rfc_hdr_t*)resp_v;
	cpu_ancount = be16_to_cpu(hdr->ancount);
	
	PERR("1:DNS:RESPONSE:ANSWER_SECTION:START\n");

	if (cpu_ancount > rrs_n_max) {
		PERR("0:DNS:RESPONSE:ANSWER_SECTION:ERROR:too many answer resource record(s) %u, max is %u\n", cpu_ancount, rrs_n_max);
		exit(1);
	}
	PERR("1:DNS:RESPONSE:ANSWER_SECTION:parsing %u resource records\n", cpu_ancount);

	r = rrs_parse(&resp_authorities_s_v, resp_answers_rrs_v,
						resp_answers_s_v, cpu_ancount);
	PERR("1:DNS:RESPONSE:ANSWER_SECTION:END\n");
	return r;
}

/* is the response question the query question ? */
static bool qtions_parse(void)
{
	struct dns_rfc_hdr_t *hdr;
	struct dns_rfc_qtion_trailer_t *trailer;
	u8 *trailer_l;

	hdr = (struct dns_rfc_hdr_t*)resp_v;

	PERR("1:DNS:RESPONSE:QUESTION_SECTION:START\n");

	if (be16_to_cpu(hdr->qdcount) != 1) {
		PERR("0:DNS:RESPONSE:QUESTION_SECTION:WARNING:question count is not 1 but %u, trying next name server\n", be16_to_cpu(hdr->qdcount));
		return false;
	}
	PERR("1:DNS:RESPONSE:QUESTION_SECTION:parsing 1 question\n");
	/*--------------------------------------------------------------------*/
	resp_qtions_s_v = resp_v + sizeof(*hdr);

	if (!name_cmp(resp_qtions_s_v, resp_v, resp_e_v, query_qtion_s_v,
							query_v, query_e_v)) {
		PERR("1:DNS:RESPONSE:QUESTION_SECTION:WARNING:name is not our name, trying next name server, wrong name follows:\n");
		name_dump(resp_qtions_s_v, resp_v, resp_e_v);
		return false;
	}
	resp_qtion_v.name = resp_qtions_s_v; /* store it for later */
	PERR("1:DNS:RESPONSE:QUESTION_SECTION:name is ok\n");
	/*--------------------------------------------------------------------*/
	trailer = name_skip(resp_qtions_s_v, resp_v, resp_e_v);

	trailer_l = (u8*)trailer + sizeof(*trailer) - 1;
	if (trailer_l >= resp_e_v) {
		PERR("1:DNS:RESPONSE:QUESTION_SECTION:WARNING:no room in the response for the question trailer, trying next name server\n");
		return false;
	}

	if (!memcmp(trailer, query_qtion_trailer_v, sizeof(*trailer))) {
		PERR("1:DNS:RESPONSE:QUESTION_SECTION:WARNING:trailer is not our trailer, trying next name server, wrong question follows:\n");
		qtion_dump(resp_qtions_s_v, resp_v, resp_e_v);
		return false;
	}
	resp_qtion_v.trailer = trailer; /* store it for later */
	PERR("1:DNS:RESPONSE:QUESTION_SECTION:trailer is ok\n");

	resp_answers_s_v = (u8*)trailer + sizeof(*trailer);

	PERR("1:DNS:RESPONSE:QUESTION_SECTION:END\n");
	return true; /* ok, its really our question */
}

static bool hdr_info_chk(void)
{
	struct dns_rfc_hdr_t *hdr;
	u16 cpu_u16;

	hdr = (struct dns_rfc_hdr_t*)resp_v;
	cpu_u16 = be16_to_cpu(hdr->info);
	PERR("1:DNS:RESPONSE:hdr=0x%04x\n", cpu_u16);

	if ((cpu_u16 & DNS_RFC_HDR_INFO_AA_BIT) != 0)
		PERR("1:DNS:RESPONSE:name server is authoritative for our query\n");
	else 
		PERR("1:DNS:RESPONSE:name server is not authoritative for our query\n");

	if ((cpu_u16 & DNS_RFC_HDR_INFO_RD_BIT) != 0)
		PERR("1:DNS:RESPONSE:recursion desired bit is set\n");
	else
		PERR("1:DNS:RESPONSE:recursion desired bit is not set\n");

	if ((cpu_u16 & DNS_RFC_HDR_INFO_RA_BIT) != 0)
		PERR("1:DNS:RESPONSE:server supports recursion\n");
	else
		PERR("1:DNS:RESPONSE:server does not support recursion\n");

	if ((cpu_u16 & DNS_RFC_HDR_INFO_Z_MASK) != 0)
		PERR("1:DNS:RESPONSE:WARNING:message z field is not 0, ignoring and continuing response parsing\n");
	
	if ((cpu_u16 & DNS_RFC_HDR_INFO_QR_BIT) == 0) {
		PERR("1:DNS:RESPONSE:WARNING:message is not a response, trying next name server\n");
		return false;
	}

	if ((cpu_u16 & DNS_RFC_HDR_INFO_OPCODE_MASK) != 0) {
		PERR("1:DNS:RESPONSE:WARNING:message has the wrong opcode, has 0x%x, expected 0, trying next name server\n", DNS_RFC_HDR_INFO_OPCODE(cpu_u16));
		return false;
	}

	if ((cpu_u16 & DNS_RFC_HDR_INFO_TC_BIT) != 0) {
		PERR("1:DNS:RESPONSE:WARNING:we do not support truncated response(yet or never), trying next name server\n");
		return false;
	}
	
	if ((cpu_u16 & DNS_RFC_HDR_INFO_RCODE_MASK) != 0) {
		PERR("1:DNS:RESPONSE:WARNING:non zero error condition 0x%x, trying next name server\n", DNS_RFC_HDR_INFO_RCODE(cpu_u16));
		return false;
	}
	return true;
}

static bool basic_and_hdr_chks(void)
{
	struct dns_rfc_hdr_t *resp_hdr;
	struct dns_rfc_hdr_t *query_hdr;

	if (resp_sz_v < sizeof(*resp_hdr)) {
		PERR("1:DNS:RESPONSE:WARNING:udp datagram cannot fit the header, hence too small, trying next name server\n");
		return false;
	}

	resp_hdr = (struct dns_rfc_hdr_t*)resp_v;
	query_hdr = (struct dns_rfc_hdr_t*)query_v;

	/* check the id filter */
	if (resp_hdr->id != query_hdr->id) {
		PERR("1:DNS:RESPONSE:WARNING:message does not have the right id, response id is 0x%x, expected id is 0x%x, trying next name server\n", resp_hdr->id, query_hdr->id);
		return false;
	}

	if (!hdr_info_chk())
		return false;
	return true;
}

#define CANONICAL_NAME_CMP(a) name_cmp(\
a, resp_v, resp_e_v, \
canonical_dn_v, canonical_dn_v, canonical_dn_v + DNS_RFC_NAME_SZ_MAX)

#define MX_EXCHANGE_CPY(mx) name_cpy(\
mx.exchange, resp_mx->exchange, resp_v, resp_e_v)

#define MX_EXCHANGE_DUMP(mx) name_dump( \
mx.exchange, \
mx.exchange, \
mx.exchange + DNS_RFC_NAME_SZ_MAX)
static void mx_store(struct rr_t *mx)
{
	struct dns_rfc_mx_t *resp_mx;

	/*
	 * the basic scan done once receiving the dns message already did 
	 * structural bound checking
	 */

	/* first, is this mx targetting our canonical dn */
	if (!CANONICAL_NAME_CMP(mx->name)) {
		PERR("1:DNS:RESPONSE:MXS:found mx resource record _not_ targetting our canonical name, name follows and skipping:\n");
		name_dump(mx->name, resp_v, resp_e_v);
		return;
	}

	/* this mx rr is targetting our canonical dn */

	resp_mx = (struct dns_rfc_mx_t*)mx->part->rdata;

	if (mxs_n_v == CONFIG_MXS_N_MAX) {
		PERR("1:DNS:MXS:ERROR:no room to store the found mx, max is %u\n", CONFIG_MXS_N_MAX);
		exit(1);
	}

	PERR("1:DNS:RESPONSE:MXS:found mx resource record targetting our canonical name\n");

	mxs_v[mxs_n_v].preference = be16_to_cpu(resp_mx->preference);
	(void)MX_EXCHANGE_CPY(mxs_v[mxs_n_v]);

	PERR("1:DNS:MX:%u:DUMP:START\n", mxs_n_v);
	PERR("1:DNS:MX:%u:DUMP:PREFERENCE:%u\n", mxs_n_v, mxs_v[mxs_n_v].preference);
	PERR("1:DNS:MX:%u:DUMP:EXCHANGE:\n", mxs_n_v);
	MX_EXCHANGE_DUMP(mxs_v[mxs_n_v]);
	PERR("1:DNS:MX:%u:DUMP:END\n", mxs_n_v);

	++mxs_n_v;
}
#undef CANONICAL_NAME_CMP
#undef MX_EXCHANGE_CPY
#undef MX_EXCHANGE_DUMP

static bool is_addr(struct rr_t *rr)
{
	if (rr->part->class != be16_to_cpu(DNS_RFC_RR_CLASS_IN))
		return false;

#ifdef CONFIG_SMTP_IPV4
	if (rr->part->type == be16_to_cpu(DNS_RFC_RR_TYPE_A)) 
		return true;
#endif
	if (rr->part->type == be16_to_cpu(DNS_RFC_RR_TYPE_AAAA))
		return true;
}

static bool addr_match_mx_exchange(struct rr_t *a, struct mx_t *mx)
{
	return name_cmp(a->name, resp_v, resp_e_v, mx->exchange, mx->exchange,
					mx->exchange + DNS_RFC_NAME_SZ_MAX);
}

static void addr_store_smtp_ip(struct rr_t *a)
{
	struct ip_t *ip;

	if (dn_v->smtp_ips_n == CONFIG_DN_SMTP_IPS_N_MAX) {
		PERR("0:DNS:RESPONSE:%.*s:no room for new ip, max of %u ips per domain name was reached\n", dn_v->dn.l - dn_v->dn.s + 1, dn_v->dn.s, CONFIG_DN_SMTP_IPS_N_MAX);
		exit(1);
	}

	ip = &dn_v->smtp_ips[dn_v->smtp_ips_n];

#ifdef CONFIG_SMTP_IPV4
	if (a->part->type == be16_to_cpu(DNS_RFC_RR_TYPE_A)) {
		ip->type = ipv4;
		memcpy(&ip->ipv4_net, a->part->rdata, sizeof(ip->ipv4_net));
		PERR("1:DNS:RESPONSE:\"%.*s\":storing smtp ipv4:0x%08lx\n", dn_v->dn.l - dn_v->dn.s + 1, dn_v->dn.s, be32_to_cpu(ip->ipv4_net));
		++(dn_v->smtp_ips_n);
		return;
	} 
	ip->type = ipv6;
#endif
	memcpy(&ip->ipv6_net[0], a->part->rdata, sizeof(ip->ipv6_net));
	PERR("1:DNS:RESPONSE:\"%.*s\":storing smtp ipv6:0x%016lx%016lx\n", dn_v->dn.l - dn_v->dn.s + 1, dn_v->dn.s, be64_to_cpu(ip->ipv6_net_h), be64_to_cpu(ip->ipv6_net_l));
	++(dn_v->smtp_ips_n);
	return;
}
/******************************************************************************/
/* exported in response.h */
/*
 * this data comes from "outside", do _not_ trust its content.
 */
static void resp_ar_collect_mx_ips(struct mx_t *mx)
{
	struct dns_rfc_hdr_t *hdr;
	u16 cpu_arcount;
	u16 i;

	hdr = (struct dns_rfc_hdr_t*)resp_v;
	cpu_arcount = be16_to_cpu(hdr->arcount);

	i = 0;
	loop {
		struct rr_t *rr;

		if (i == cpu_arcount)
			break;

		rr = &resp_additionals_rrs_v[i];

		if (is_addr(rr) && addr_match_mx_exchange(rr, mx))
			addr_store_smtp_ip(rr);
		++i;
	}
}

static void resp_an_collect_mx_ips(struct mx_t *mx)
{
	struct dns_rfc_hdr_t *hdr;
	u16 cpu_ancount;
	u16 i;

	hdr = (struct dns_rfc_hdr_t*)resp_v;
	cpu_ancount = be16_to_cpu(hdr->ancount);

	i = 0;
	loop {
		struct rr_t *rr;

		if (i == cpu_ancount)
			break;

		rr = &resp_answers_rrs_v[i];

		if (is_addr(rr) && addr_match_mx_exchange(rr, mx))
			addr_store_smtp_ip(rr);
		++i;
	}
}

static void resp_an_collect_canonical_dn_ips(void)
{
	struct dns_rfc_hdr_t *hdr;
	u16 cpu_ancount;
	u16 i;

	hdr = (struct dns_rfc_hdr_t*)resp_v;
	cpu_ancount = be16_to_cpu(hdr->ancount);

	i = 0;
	loop {
		struct rr_t *rr;

		if (i == cpu_ancount)
			break;

		rr = &resp_answers_rrs_v[i];

		if (is_addr(rr) && name_cmp(rr->name, resp_v, resp_e_v,
						canonical_dn_v, canonical_dn_v,
							canonical_dn_e_v)) 
			addr_store_smtp_ip(rr);
		++i;
	}
}

static void resp_ar_collect_canonical_dn_ips(void)
{
	struct dns_rfc_hdr_t *hdr;
	u16 cpu_arcount;
	u16 i;

	hdr = (struct dns_rfc_hdr_t*)resp_v;
	cpu_arcount = be16_to_cpu(hdr->arcount);

	i = 0;
	loop {
		struct rr_t *rr;

		if (i == cpu_arcount)
			break;

		rr = &resp_additionals_rrs_v[i];

		if (is_addr(rr) && name_cmp(rr->name, resp_v, resp_e_v,
						canonical_dn_v, canonical_dn_v,
							canonical_dn_e_v)) 
			addr_store_smtp_ip(rr);
		++i;
	}
}

static bool resp_basic_parse(void)
{
	PERR("1:DNS:RESPONSE:received udp datagram of %lu bytes\n", resp_sz_v);

	if (!basic_and_hdr_chks())
		return false;
	if (!qtions_parse())
		return false;
	if (!answers_parse())
		return false;
	if (!authorities_parse())
		return false;
	if (!additionals_parse())
		return false;
	return true;
}

/*
 * scan the answer section for a cname rr which defines the canonical domain
 * name for our question domain name
 */
static bool resp_cname_rr_scan(void)
{
	struct dns_rfc_hdr_t *hdr;
	u16 ancount;
	u16 i;

	hdr = (struct dns_rfc_hdr_t*)resp_v;
	ancount = be16_to_cpu(hdr->ancount);

	i = 0;
	loop {
		struct rr_t *rr;

		if (i == ancount)
			break;

		rr = &resp_answers_rrs_v[i];

		if (	   rr->part->class == cpu_to_be16(DNS_RFC_RR_CLASS_IN)
			&& rr->part->type == cpu_to_be16(DNS_RFC_RR_TYPE_CNAME)
			&& rr->part->ttl != 0
			&& rr->part->rdlength != 0
			&& name_cmp(rr->name, resp_v, resp_e_v,
					resp_qtion_v.name, resp_v, resp_e_v)) {
			cname_rr_v = rr;
			return true;
		}
		++i;
	}
	return false;
}

static void resp_mxs_scan(void)
{
	struct dns_rfc_hdr_t *hdr;
	u16 ancount;
	u16 i;

	hdr = (struct dns_rfc_hdr_t*)resp_v;
	ancount = be16_to_cpu(hdr->ancount);

	mxs_n_v = 0;
	i = 0;
	PERR("1:DNS:********************************************************************************\n");
	PERR("1:DNS:RESPONSE:MXS:SCANNING:START\n");
	loop {
		struct rr_t *rr;

		if (i == ancount)
			break;

		rr = &resp_answers_rrs_v[i];
		if (rr->part->class = cpu_to_be16(DNS_RFC_RR_CLASS_IN)	
			&& rr->part->type == cpu_to_be16(DNS_RFC_RR_TYPE_MX)) {
			mx_store(rr);
		}
		++i;
	}
	PERR("1:DNS:RESPONSE:MXS:SCANNING:END\n");
	PERR("1:DNS:********************************************************************************\n");
}
/*----------------------------------------------------------------------------*/
#define CLEANUP
#include "namespace/ulinux.h"
#include "namespace/syncsm.h"
#include "dns/namespace/state.h"
#include "dns/namespace/resolver.h"
#include "dns/namespace/name.h"
#include "dns/namespace/response.h"
#include "dns/namespace/response.c"
#undef CLEANUP
#endif
