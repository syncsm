#ifndef SYNCSM_DNS_RESPONSE_H
#define SYNCSM_DNS_RESPONSE_H
#include "dns/namespace/response.h"
/*
 * this code is protected by the GNU affero GPLv3 license
 * author:Sylvain BERTRAND
 */
static void resp_an_collect_canonical_dn_ips(void);
static void resp_an_collect_mx_ips(struct mx_t *mx);
static void resp_ar_collect_canonical_dn_ips(void);
static void resp_ar_collect_mx_ips(struct mx_t *mx);
static bool resp_basic_parse(void);
static bool resp_cname_rr_scan(void);
static void resp_mxs_scan(void);
#define CLEANUP
#include "dns/namespace/response.h"
#undef CLEANUP
#endif
