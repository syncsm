#ifndef DNS_RFC_H
#define DNS_RFC_H
/*
 * this code is protected by the GNU affero GPLv3 license
 * author:Sylvain BERTRAND
 */
#include "namespace/ulinux.h"
/*----------------------------------------------------------------------------*/
/* universe global, short enough, no namespace */
#define DNS_RFC_UDP_PAYLOAD_SZ_MAX 512
#define DNS_RFC_PORT 53 /* could be tcp */
#define DNS_RFC_NAME_SZ_MAX 255
#define DNS_RFC_LABEL_SZ_MAX 63

#define DNS_RFC_RR_TYPE_A	1
#define DNS_RFC_RR_TYPE_NS	2
#define DNS_RFC_RR_TYPE_MD	3
#define DNS_RFC_RR_TYPE_MF	4
#define DNS_RFC_RR_TYPE_CNAME	5
#define DNS_RFC_RR_TYPE_SOA	6
#define DNS_RFC_RR_TYPE_MB	7
#define DNS_RFC_RR_TYPE_MG	8
#define DNS_RFC_RR_TYPE_MR	9
#define DNS_RFC_RR_TYPE_NULL	10
#define DNS_RFC_RR_TYPE_WKS	11
#define DNS_RFC_RR_TYPE_PTR	12
#define DNS_RFC_RR_TYPE_HINFO	13
#define DNS_RFC_RR_TYPE_MINFO	14
#define DNS_RFC_RR_TYPE_MX	15
#define DNS_RFC_RR_TYPE_TXT	16
#define DNS_RFC_RR_TYPE_AAAA	28

#define DNS_RFC_RR_CLASS_IN 1
#define DNS_RFC_RR_CLASS_CN 2
#define DNS_RFC_RR_CLASS_CH 3
#define DNS_RFC_RR_CLASS_HS 4

#define DNS_RFC_LABEL_OFFSET_IS_PTR_MARK 0xc0
#define DNS_RFC_LABEL_PTR_OFFSET_MASK 0x3fff

#define DNS_RFC_HDR_INFO_QR_BIT (1 << 15)
#define DNS_RFC_HDR_INFO_OPCODE_MASK 0x7800
#define DNS_RFC_HDR_INFO_OPCODE(x) (((x) & DNS_RFC_HDR_INFO_OPCODE_MASK) >> 11)
#define DNS_RFC_HDR_INFO_AA_BIT (1 << 10)
#define DNS_RFC_HDR_INFO_TC_BIT (1 << 9)
#define DNS_RFC_HDR_INFO_RD_BIT (1 << 8)
#define DNS_RFC_HDR_INFO_RA_BIT (1 << 7)
#define DNS_RFC_HDR_INFO_Z_MASK 0x0070
#define DNS_RFC_HDR_INFO_RCODE_MASK 0x000f
#define DNS_RFC_HDR_INFO_RCODE(x) ((x) & DNS_RFC_HDR_INFO_RCODE_MASK)
struct dns_rfc_hdr_t {
	u16 id;
	u16 info;
	u16 qdcount;
	u16 ancount;
	u16 nscount;
	u16 arcount;
} PACKED;
/*----------------------------------------------------------------------------*/
struct dns_rfc_qtion_trailer_t {
	u16 type;
	u16 class;
} PACKED;
/*----------------------------------------------------------------------------*/
struct dns_rfc_rr_part_t {
	u16 type;
	u16 class;
	s32 ttl;
	u16 rdlength;
	u8 rdata[];
} PACKED;
/*----------------------------------------------------------------------------*/
struct dns_rfc_mx_t {
	u16 preference;
	u8 exchange[];
} PACKED;
/*----------------------------------------------------------------------------*/
#define CLEANUP
#include "namespace/ulinux.h"
#undef CLEANUP
#endif
