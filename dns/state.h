#ifndef SYNCSM_DNS_STATE_H
#define SYNCSM_DNS_STATE_H
/*
 * this code is protected by the GNU affero GPLv3 license
 * author:Sylvain BERTRAND
 */
#include "namespace/ulinux.h"
#include "namespace/syncsm.h"
#include "dns/namespace/state.h"
/*----------------------------------------------------------------------------*/
static struct dn_al_t *dn_v; /* current targetted dn */

/* the canonical domain name which since the targetted dn could be an alias */
static u8 canonical_dn_v[DNS_RFC_NAME_SZ_MAX];
static u8 *canonical_dn_e_v;

/*
 * if mxs are provided, store them in order to get their A (if smtp ipv4 is 
 * enabled) and AAAA
 */
struct mx_t {
	u16 preference;
	u8 exchange[DNS_RFC_NAME_SZ_MAX];
};
static struct mx_t mxs_v[CONFIG_MXS_N_MAX];
static u16 mxs_n_v;
/*----------------------------------------------------------------------------*/
/*
 * Some dns servers do support _only_ recursive query mode (for instance
 * cisco wifi routers used in many starbucks). They do not support the default
 * mandatory iterative query mode.
 * Whatever, from a client point of view, it should be simple to query
 * the dns, hence the recursive mode should have been the default and only mode
 * since the beginning, from a client point of view. The iterative mode should
 * actually be used only by the network of dns servers.
 */
static struct ip_t		nss_v[CONFIG_DNS_NSS_N_MAX];
static u8			nss_n_v;
static u8			ns_v; /* current targetted ns */
/*----------------------------------------------------------------------------*/
/******************************************************************************/
/******************************************************************************/
static u16 id_v;
/*============================================================================*/
/* query/question */
static u8	qtion_dn_v[DNS_RFC_NAME_SZ_MAX];
static u8	*qtion_dn_e_v;
static u16	qtion_type_v; /* mx? a? aaaa? */

static u8				query_v[DNS_RFC_UDP_PAYLOAD_SZ_MAX];
static u8				*query_qtion_s_v;
static struct dns_rfc_qtion_trailer_t	*query_qtion_trailer_v;
static u16				query_qtion_sz_v;
static u8 				*query_qtion_e_v; /* comfort variable */
static u16				query_sz_v;
static u8 				*query_e_v; /* comfort variable */
/*============================================================================*/
/* response */
static u8		resp_v[DNS_RFC_UDP_PAYLOAD_SZ_MAX];
static u16		resp_sz_v;
static u8		*resp_e_v; /* comfort variable */
/*----------------------------------------------------------------------------*/
struct resp_qtion_t {
	u8 *name;
	struct dns_rfc_qtion_trailer_t *trailer;
};
static u8		*resp_qtions_s_v;
static struct resp_qtion_t resp_qtion_v; /* we are only 1 question */
/*----------------------------------------------------------------------------*/
enum { rrs_n_max = 100 };
struct rr_t {
	u8				*name;
	struct dns_rfc_rr_part_t	*part;
};
/*----------------------------------------------------------------------------*/
static u8		*resp_answers_s_v;
static struct rr_t	resp_answers_rrs_v[rrs_n_max];

static struct rr_t	*cname_rr_v; /* the first cname in this section */
/*----------------------------------------------------------------------------*/
static u8		*resp_authorities_s_v;
static struct rr_t	resp_authorities_rrs_v[rrs_n_max];
/*----------------------------------------------------------------------------*/
static u8		*resp_additionals_s_v;
static struct rr_t	resp_additionals_rrs_v[rrs_n_max];
/******************************************************************************/
/******************************************************************************/
#define CLEANUP
#include "namespace/ulinux.h"
#include "namespace/syncsm.h"
#include "dns/namespace/state.h"
#undef CLEANUP
#endif
