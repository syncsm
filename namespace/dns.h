/*
 * this code is protected by the GNU affero GPLv3 license
 * author:Sylvain BERTRAND
 */
#ifndef CLEANUP
#define dns_init     syncsm_dns_init
#define dns_resolver syncsm_dns_resolver
/******************************************************************************/
#else /* CLEANUP */
#undef dns_init
#undef dns_resolver
#endif
