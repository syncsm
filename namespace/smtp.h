/*
 * this code is protected by the GNU affero GPLv3 license
 * author:Sylvain BERTRAND
 */
#ifndef CLEANUP
#define smtp_init syncsm_smtp_init
#define smtp_send syncsm_smtp_send
/******************************************************************************/
#else /* CLEANUP */
#undef smtp_init
#undef smtp_send
#endif
