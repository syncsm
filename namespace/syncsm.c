/*
 * this code is protected by the GNU affero GPLv3 license
 * author:Sylvain BERTRAND
 */
#ifndef CLEANUP
#define dn_al_eq                         syncsm_c_dn_al_eq
#define dn_al_insert                     syncsm_c_dn_al_insert
#define dn_al_locate                     syncsm_c_dn_al_locate
#ifdef CONFIG_DISABLE_SMTP_TRANSPARENCY
#define email_read_from_stdin            syncsm_c_email_read_from_stdin
#else
#define stdin_read                       syncsm_c_stdin_read
#define email_write_byte                 syncsm_c_email_write_byte
#define email_write_bytes                syncsm_c_email_write_bytes
#define match                            syncsm_c_match
#define normal                           syncsm_c_normal
#define flush_partial_terminator         syncsm_c_flush_partial_terminator
#define matching_terminator              syncsm_c_matching_terminator
#define email_read_and_escape_from_stdin syncsm_c_email_read_and_escape_from_stdin
#endif
#define globals_init                     syncsm_c_globals_init
#define ip_eq                            syncsm_c_ip_eq
#define rcpt_dn_al_process               syncsm_c_rcpt_dn_al_process
#define rcpt_prepare                     syncsm_c_rcpt_prepare
#define rcpt_slices	                 syncsm_c_rcpt_slices
#define rcpts		                 syncsm_c_rcpts
#define rcpts_locate_from                syncsm_c_rcpts_locate_from
#define rcpts_prepare                    syncsm_c_rcpts_prepare
#define slice_eq                         syncsm_c_slice_eq
#define stdin_nonblock_status            syncsm_c_stdin_nonblock_status
#define to_ipv6                          syncsm_c_to_ipv6
/******************************************************************************/
#else /* CLEANUP */
#undef dn_al_desc
#undef dn_al_insert
#undef dn_al_locate
#ifdef CONFIG_DISABLE_SMTP_TRANSPARENCY
#undef email_read_from_stdin
#else
#undef stdin_read
#undef email_write_byte
#undef email_write_bytes
#undef match
#undef normal
#undef flush_partial_terminator
#undef matching_terminator
#undef email_read_and_escape_from_stdin
#endif
#undef globals_init
#undef ip_eq
#undef rcpt_dn_al_process
#undef rcpt_prepare
#undef rcpt_slices
#undef rcpts
#undef rcpts_locate_from
#undef rcpts_prepare
#undef slice_eq
#undef stdin_nonblock_status
#undef to_ipv6
#endif
