/*
 * this code is protected by the GNU affero GPLv3 license
 * author:Sylvain BERTRAND
 */
#ifndef CLEANUP
#define dn_al_t     syncsm_dn_al_t
#define dn_al_v     syncsm_dn_al_v
#define dn_al_n_v   syncsm_dn_al_n_v
#define email_v     syncsm_email_v
#define email_sz_v  syncsm_email_sz_v
#define ip_t        syncsm_ip_t
#define ipv4        syncsm_ipv4
#define ipv6        syncsm_ipv6
#define str_slice_t syncsm_str_slice_t
/******************************************************************************/
#else /* CLEANUP */
#undef dn_al_t
#undef dn_al_v
#undef dn_al_n_v
#undef email_v
#undef email_sz_v
#undef ip_t
#undef ipv4
#undef ipv6
#undef str_slice_t
#endif
