/*
 * this code is protected by the GNU affero GPLv3
 * author:Sylvain BERTRAND
 */
#ifndef CLEANUP
#define loop for(;;)
#define sl ulinux_sl
#define si ulinux_si
#define ul ulinux_ul
#define u8 ulinux_u8
#define s8 ulinux_s8
#define u16 ulinux_u16
#define s16 ulinux_s16
#define s32 ulinux_s32
#define u32 ulinux_u32
#define s64 ulinux_s64
#define u64 ulinux_u64
/*----------------------------------------------------------------------------*/
#define dprintf ulinux_dprintf
#define snprintf ulinux_snprintf
/*----------------------------------------------------------------------------*/
#define EAGAIN ULINUX_EAGAIN
#define EINTR ULINUX_EINTR
#define ISERR ULINUX_ISERR
/*----------------------------------------------------------------------------*/
#define exit(a) ulinux_sysc(exit_group,1,a)
/*----------------------------------------------------------------------------*/
#define read(a,b,c) ulinux_sysc(read,3,a,b,c)
#define write(a,b,c) ulinux_sysc(write,3,a,b,c)
/*----------------------------------------------------------------------------*/
#define PROT_READ ULINUX_PROT_READ
#define PROT_WRITE ULINUX_PROT_WRITE
#define MAP_ANONYMOUS ULINUX_MAP_ANONYMOUS
#define MAP_UNINITIALIZED ULINUX_MAP_UNINITIALIZED
#define MAP_PRIVATE ULINUX_MAP_PRIVATE
#define mmap(addr,sz,attr,flgs,fd,off) ulinux_sysc(mmap,6,addr,sz,attr,flgs,fd,off)
/*----------------------------------------------------------------------------*/
#define AT_CWD ULINUX_AT_CWD
#define O_RD_ONLY ULINUX_O_RDONLY
#define O_NONBLOCK ULINUX_O_NONBLOCK
#define openat(a,b,c,d) ulinux_sysc(openat,4,a,b,c,d)
/*----------------------------------------------------------------------------*/
#define stat ulinux_stat
#define fstat(a,b) ulinux_sysc(fstat,2,a,b)
/*----------------------------------------------------------------------------*/
#define close(a) ulinux_sysc(close,1,a)
/*----------------------------------------------------------------------------*/
#define F_SETFL ULINUX_F_SETFL
#define F_GETFL ULINUX_F_GETFL
#define fcntl(a,b,c) ulinux_sysc(fcntl,3,a,b,c)
/*----------------------------------------------------------------------------*/
#define memset ulinux_memset
#define memcmp ulinux_memcmp
#define memcpy ulinux_memcpy
/*----------------------------------------------------------------------------*/
#define is_digit ulinux_is_digit
#define is_space ulinux_is_space
#define is_print ulinux_is_print
#define to_ipv4_blk ulinux_to_ipv4_blk
#define to_ipv6_blk ulinux_to_ipv6_blk
#define cpu_to_be16 ulinux_cpu_to_be16
#define be16_to_cpu ulinux_be16_to_cpu
#define be32_to_cpu ulinux_be32_to_cpu
#define be64_to_cpu ulinux_be64_to_cpu
/*----------------------------------------------------------------------------*/
#if defined CONFIG_DNS_IPV4 || defined CONFIG_SMTP_IPV4
	#define AF_INET ULINUX_AF_INET
	#define sockaddr_in ulinux_sockaddr_in
#endif
#define AF_INET6 ULINUX_AF_INET6
#define IPPROTO_TCP ULINUX_IPPROTO_TCP
#define TCP_USER_TIMEOUT ULINUX_TCP_USER_TIMEOUT
#define setsockopt(a,b,c,d,e) ulinux_sysc(setsockopt,5,a,b,c,d,e)
#define getsockopt(a,b,c,d,e) ulinux_sysc(getsockopt,5,a,b,c,d,e)
#define sockaddr_in6 ulinux_sockaddr_in6
#define SOCK_DGRAM ULINUX_SOCK_DGRAM
#define SOCK_STREAM ULINUX_SOCK_STREAM
#define SOCK_O_NONBLOCK ULINUX_O_NONBLOCK
#define socket(a,b,c) ulinux_sysc(socket,3,a,b,c)
#define EINPROGRESS ULINUX_EINPROGRESS
#define connect(a,b,c) ulinux_sysc(connect,3,a,b,c)
#define recvfrom(a,b,c,d,e,f) ulinux_sysc(recvfrom,6,a,b,c,d,e,f)
/*----------------------------------------------------------------------------*/
#define CLOCK_MONOTONIC ULINUX_CLOCK_MONOTONIC
#define TFD_NONBLOCK ULINUX_TFD_NONBLOCK
#define itimerspec ulinux_itimerspec
#define timerfd_create(a,b) ulinux_sysc(timerfd_create,2,a,b)
#define timerfd_settime(a,b,c,d) ulinux_sysc(timerfd_settime,4,a,b,c,d)
#define epoll_create1(a) ulinux_sysc(epoll_create1,1,a)
#define epoll_event ulinux_epoll_event
#define EPOLLIN ULINUX_EPOLLIN
#define epoll_ctl(a,b,c,d) ulinux_sysc(epoll_ctl,4,a,b,c,d)
#define EPOLL_CTL_ADD ULINUX_EPOLL_CTL_ADD
#define EPOLL_CTL_DEL ULINUX_EPOLL_CTL_DEL
#define epoll_pwait(a,b,c,d,e) ulinux_sysc(epoll_pwait,5,a,b,c,d,e)
/*----------------------------------------------------------------------------*/
#define QSORT_CMP_LT ULINUX_QSORT_CMP_LT
#define QSORT_CMP_EQ ULINUX_QSORT_CMP_EQ
#define QSORT_CMP_GT ULINUX_QSORT_CMP_GT
#define qsort ulinux_qsort
/******************************************************************************/
#else /* CLEANUP */
#undef loop
#undef sl
#undef si
#undef ul
#undef u8
#undef s8
#undef u16
#undef s16
#undef s32
#undef u32
#undef s64
#undef u64
#undef dprintf
#undef EAGAIN
#undef EINTR
#undef ISERR
#undef exit
#undef read
#undef write
#undef PROT_READ
#undef PROT_WRITE
#undef MAP_ANONYMOUS
#undef MAP_UNINITIALIZED
#undef MAP_PRIVATE
#undef mmap
#undef AT_CWD
#undef O_RD_ONLY
#undef O_NONBLOCK
#undef openat
#undef stat
#undef fstat
#undef close
#undef F_SETFL
#undef F_GETFL
#undef fcntl
#undef memset
#undef memcmp
#undef memcpy
#undef is_digit
#undef is_space
#undef is_print
#undef to_ipv4_blk
#undef to_ipv6_blk
#undef cpu_to_be16
#undef be16_to_cpu
#undef be32_to_cpu
#undef be64_to_cpu
#if defined CONFIG_DNS_IPV4 || defined CONFIG_SMTP_IPV4
	#undef AF_INET
	#undef sockaddr_in
#endif
#undef AF_INET6
#undef IPPROTO_TCP
#undef TCP_USER_TIMEOUT
#undef setsockopt
#undef getsockopt
#undef sockaddr_in6
#undef SOCK_DGRAM
#undef SOCK_O_NONBLOCK
#undef socket
#undef EINPROGRESS
#undef connect
#undef recvfrom
#undef CLOCK_MONOTONIC
#undef TFD_NONBLOCK
#undef itimerspec
#undef timerfd_create
#undef timerfd_settime
#undef epoll_create1
#undef epoll_event
#undef EPOLLIN
#undef epoll_ctl
#undef EPOLL_CTL_ADD
#undef epoll_pwait
#undef QSORT_CMP_LT
#undef QSORT_CMP_EQ
#undef QSORT_CMP_GT
#undef qsort
#endif
