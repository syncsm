#ifndef PERR_H
#define PERR_H
/*
 * this code is protected by the GNU affero GPLv3 license
 * author:Sylvain BERTRAND
 */
#include "namespace/ulinux.h"
/* secure a 0 terminating byte */
#define PERR(fmt,...) dprintf(2, &syncsm_dprint_buf[0], CONFIG_BUFSIZ - 1, fmt, ##__VA_ARGS__)
#define CSTRLEN(cstr) (sizeof(cstr) - 1)
static u8 syncsm_dprint_buf[CONFIG_BUFSIZ];
#define CLEANUP
#include "namespace/ulinux.h"
#undef CLEANUP
#endif
