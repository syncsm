#ifndef SYNCSM_SMTP_H
#define SYNCSM_SMTP_H
/*
 * this code is protected by the GNU affero GPLv3 license
 * author:Sylvain BERTRAND
 */
#include "namespace/smtp.h"
/*----------------------------------------------------------------------------*/
static void smtp_init(void);
static void smtp_send(void);
/*----------------------------------------------------------------------------*/
#define CLEANUP
#include "namespace/smtp.h"
#undef CLEANUP
#endif
