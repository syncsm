/*
 * this code is protected by the GNU affero GPLv3 license
 * author:Sylvain BERTRAND
 */
#ifndef CLEANUP
#define cmd                      syncsm_smtp_smtp_c_cmd
#define cmd_data                 syncsm_smtp_smtp_c_cmd_data
#define cmd_e                    syncsm_smtp_smtp_c_cmd_e
#define cmd_ehlo                 syncsm_smtp_smtp_c_cmd_ehlo
#define cmd_mail_from            syncsm_smtp_smtp_c_cmd_mail_from
#define cmd_quit                 syncsm_smtp_smtp_c_cmd_quit
#define cmd_rcpt                 syncsm_smtp_smtp_c_cmd_rcpt
#define dn_al                    syncsm_smtp_smtp_c_dn_al
#define epfd                     syncsm_smtp_smtp_c_epfd
#define ip                       syncsm_smtp_smtp_c_ip
#define mail_send                syncsm_smtp_smtp_c_mail_send
#define perr_cmd                 syncsm_smtp_smtp_c_perr_cmd
#define perr_ipvx                syncsm_smtp_smtp_c_perr_ipvx
#define perr_reply_line          syncsm_smtp_smtp_c_perr_reply_line
#define reply_data               syncsm_smtp_smtp_c_reply_data
#define reply_data_e             syncsm_smtp_smtp_c_reply_data_e
#define reply_data_p             syncsm_smtp_smtp_c_reply_data_p
#define reply_data_recv          syncsm_smtp_smtp_c_reply_data_recv
#define reply_line               syncsm_smtp_smtp_c_reply_line
#define reply_line_e             syncsm_smtp_smtp_c_reply_line_e
#define reply_line_next          syncsm_smtp_smtp_c_reply_line_next
#define reply_xxx                syncsm_smtp_smtp_c_reply_xxx
#define sa                       syncsm_smtp_smtp_c_sa
#define sa_init                  syncsm_smtp_smtp_c_sa_init
#define send_cmd                 syncsm_smtp_smtp_c_send_cmd
#define server_send              syncsm_smtp_smtp_c_server_send
#define servers_send             syncsm_smtp_smtp_c_servers_send
#define so                       syncsm_smtp_smtp_c_so
#define so_create                syncsm_smtp_smtp_c_so_create
#define so_recv                  syncsm_smtp_smtp_c_so_recv
#define so_send                  syncsm_smtp_smtp_c_so_send
#define so_shutdown              syncsm_smtp_smtp_c_so_shutdown
#define so_user_timeout          syncsm_smtp_smtp_c_so_user_timeout
#define tcp_connect              syncsm_smtp_smtp_c_tcp_connect
#define tcp_send_timeout_restore syncsm_smtp_smtp_c_tcp_send_timeout_restore
#define tcp_send_timeout_set     syncsm_smtp_smtp_c_tcp_send_timeout_set
#define timer_expired            syncsm_smtp_smtp_c_timer_expired
#define timer_setup              syncsm_smtp_smtp_c_timer_setup
#define timer_start              syncsm_smtp_smtp_c_timer_start
#define timerfd                  syncsm_smtp_smtp_c_timerfd
#define user_timeout_once        syncsm_smtp_smtp_c_user_timeout_once
/******************************************************************************/
#else /* CLEANUP */
#undef cmd
#undef cmd_data
#undef cmd_e
#undef cmd_ehlo
#undef cmd_mail_from
#undef cmd_quit
#undef cmd_rcpt
#undef dn_al
#undef epfd
#undef ip
#undef mail_send
#undef perr_cmd
#undef perr_ipvx
#undef perr_reply_line
#undef reply_data
#undef reply_data_e
#undef reply_data_p
#undef reply_data_recv
#undef reply_line
#undef reply_line_e
#undef reply_line_next
#undef reply_xxx
#undef sa
#undef sa_init
#undef send_cmd
#undef servers_send
#undef so
#undef so_create
#undef so_recv
#undef so_send
#undef so_shutdown
#undef so_user_timeout
#undef tcp_connect
#undef tcp_send_timeout_restore
#undef tcp_send_timeout_set
#undef timer_expired
#undef timer_setup
#undef timer_start
#undef timerfd
#undef user_timeout_once
#endif
