#ifndef SMTP_RFC_H
#define SMTP_RFC_H
/*
 * this code is protected by the GNU affero GPLv3 license
 * author:Sylvain BERTRAND
 */
/* minimum of 100 recipients per mail */
#define SMTP_RFC_RCPTS_N_MAX 100
#define SMTP_RFC_PORT 25
#define SMTP_RFC_REPLY_LINE_N_MAX 512
#define SMTP_RFC_COMMAND_LINE_N_MAX 512
#define SMTP_RFC_INITIAL_REPLY_TIMEOUT_SEC (5 * 60)
#define SMTP_RFC_COMMAND_MAIL_FROM_REPLY_TIMEOUT_SEC (5 * 60)
#define SMTP_RFC_COMMAND_RCPT_TO_REPLY_TIMEOUT_SEC (5 * 60)
#define SMTP_RFC_COMMAND_DATA_REPLY_TIMEOUT_SEC (2 * 60)
#define SMTP_RFC_DATA_TERMINATION_REPLY_TIMEOUT_SEC (10 * 60)
#define SMTP_RFC_DATA_BLOCK_TIMEOUT_MSEC (3 * 60 * 1000)

#define SMTP_RFC_TERMINATOR "\r\n.\r\n"
#define SMTP_RFC_TERMINATOR_ESCAPE "\r\n..\r\n"
#endif

