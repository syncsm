#ifndef SYNCSM_SMTP_SMTP_C
#define SYNCSM_SMTP_SMTP_C
#include <stdbool.h>
#include <stdarg.h>
/*----------------------------------------------------------------------------*/
#include "config.h"
/*----------------------------------------------------------------------------*/
#include "ulinux.h"
/*----------------------------------------------------------------------------*/
#include "dns/rfc.h"
#include "smtp/rfc.h"
/*----------------------------------------------------------------------------*/
#include "perr.h"
#include "syncsm.h"
/*============================================================================*/
#include "namespace/ulinux.h"
#include "namespace/syncsm.h"
#include "namespace/smtp.h"
#include "smtp/namespace/smtp.c"
/*----------------------------------------------------------------------------*/
static struct dn_al_t *dn_al;
static struct ip_t *ip;
static si epfd;
static si timerfd;
static si so;
static si so_user_timeout; /* backup of the current system value */
static bool user_timeout_once; /* info about the timeout only 1 time */
/* we share this struct with the _smaller_ ipv4 version */
static struct sockaddr_in6 sa;

static u8 reply_line[SMTP_RFC_REPLY_LINE_N_MAX];
static u8 *reply_line_e;

static u8 reply_data[SMTP_RFC_REPLY_LINE_N_MAX];
static u8 *reply_data_p; /* reply data processing tracker */ 
static u8 *reply_data_e;

static u8 cmd[SMTP_RFC_COMMAND_LINE_N_MAX];
static u8 *cmd_e;
/*----------------------------------------------------------------------------*/
static void sa_init(void)
{
#ifdef CONFIG_SMTP_IPV4
	if (ip->type == ipv4) {
		struct sockaddr_in *sa_ipv4;

		sa_ipv4 = (struct sockaddr_in*)&sa;

		memset(sa_ipv4, 0, sizeof(*sa_ipv4));

		sa_ipv4->family = AF_INET;
		sa_ipv4->port = cpu_to_be16(SMTP_RFC_PORT);
		memcpy(&sa_ipv4->addr, &ip->ipv4_net, sizeof(ip->ipv4_net));
	} else if (ip->type == ipv6) {
#endif
		memset(&sa, 0, sizeof(sa));

		sa.family = AF_INET6;
		sa.port = cpu_to_be16(SMTP_RFC_PORT);
		memcpy(&sa.addr, &ip->ipv6_net, sizeof(ip->ipv6_net));
#ifdef CONFIG_SMTP_IPV4
	}
#endif
}

static void perr_ipvx(void)
{
#ifdef CONFIG_SMTP_IPV4
	if (ip->type == ipv4) {
		PERR("ipv4(0x%08lx)", be32_to_cpu(ip->ipv4_net));
	} else if (ip->type == ipv6) {
#endif
		PERR("ipv6(0x%016lx%016lx)", be64_to_cpu(ip->ipv6_net_h), be64_to_cpu(ip->ipv6_net_l));
#ifdef CONFIG_SMTP_IPV4
	}
#endif
}

static bool so_create(void)
{
	sl r;
	sl af_inet;
	
	struct epoll_event evts;

#ifdef CONFIG_SMTP_IPV4
	if (ip->type == ipv4) {
		af_inet = AF_INET;
	} else if (ip->type == ipv6) {
#endif
		af_inet = AF_INET6;
#ifdef CONFIG_SMTP_IPV4
	}
#endif

	r = socket(af_inet, SOCK_O_NONBLOCK | SOCK_STREAM, 0);
	if (ISERR(r)) {
		PERR("1:SMTP:");perr_ipvx();PERR(":WARNING:%ld:unable to create tcp socket, trying next server\n", r);
		goto err;
	}

	so = (si)r;

	memset(&evts, 0, sizeof(evts));
	evts.events = EPOLLIN;
	evts.data.fd = so;
	r = epoll_ctl(epfd, EPOLL_CTL_ADD, so, &evts);
	if (ISERR(r)) {
		PERR("1:SMTP:");perr_ipvx();PERR(":WARNING:%ld:unable to add the tcp socket file descriptor to the epoll file descriptor, trying next server\n", r);
		goto err_close_socket;
	}
	return true;

err_close_socket:
	r = close(so);
	if (ISERR(r))
		{PERR("1:SMTP:");perr_ipvx();PERR(":WARNING:%ld:unable to close the tcp socket file descriptor\n", r);}
err:
	return false;
}

static void so_shutdown(void)
{
	sl r;
 
	r = epoll_ctl(epfd, EPOLL_CTL_DEL, so, 0);
	if (ISERR(r))
		{PERR("1:SMTP:");perr_ipvx();PERR(":WARNING:%ld:unable to remove the server socket from the epoll file descriptor\n", r);}

	r = close(so);
	if (ISERR(r))
		{PERR("1:SMTP:");perr_ipvx();PERR(":WARNING:%ld:unable to close the server socket\n", r);}
}

static bool tcp_connect(void)
{
	sl r;

	sa_init();
	if (!so_create())
		return false;

	r = connect(so, &sa, sizeof(sa));
	if (ISERR(r)) {
		if ((r != -EAGAIN) && (r != -EINPROGRESS) && (r != -EINTR)) {
			PERR("1:SMTP:");perr_ipvx();PERR(":WARNING:%ld:unable to configure the tcp socket for this server, trying next one\n", r);
			so_shutdown();
			return false;
		}
	}
	PERR("1:SMTP:");perr_ipvx();PERR(":tcp socket is connected\n");
	return true;
}

static void timer_start(u64 s, u64 ns)
{
	sl r;
	struct itimerspec itimerspec;

	memset(&itimerspec, 0, sizeof(itimerspec));

	/* initial expiration in the futur of current monotonic clock */
	itimerspec.value.sec = s;
	itimerspec.value.nsec = ns;

	/* relative initial expiration */
	r = timerfd_settime(timerfd, 0, &itimerspec, 0);
	if (ISERR(r)) {
		PERR("0:SMTP:");perr_ipvx();PERR(":ERROR:%ld:unable to reset and arm our timer for %lu sec and %lu nanosec\n", r, s, ns);
		exit(1);
	}
}

static bool timer_expired(void)
{
	sl r;
	u64 expirations_n; /* the count of expirations of our timer */

	expirations_n = 0;

	loop {/* reads are atomic or err, aka no short reads */
		r = read(timerfd, &expirations_n, sizeof(u64));
		if (r != -EINTR);
			break;
	}

	if (ISERR(r)) {
		PERR("0:SMTP:");perr_ipvx();PERR(":ERROR:TIMER:%ld:unable reading the count of expirations\n", r);
		exit(1);
	}
	PERR("1:SMTP:");perr_ipvx();PERR(":TIMER:count of expirations=%lu, trying next server\n", expirations_n);

	if (expirations_n != 0)
		return true;
	return false;
}

static bool so_recv(void)
{
	sl  r;

	reply_data_e = reply_data;
	reply_data_p = reply_data; /* reply data processing tracker */
	loop {
		r = recvfrom(so, reply_data, SMTP_RFC_REPLY_LINE_N_MAX, 0 ,0
									,0);
		/*
		 * the epoll event told us we have data, do insist till we get
		 * this data
		 */
		if (r == -EAGAIN || r == -EINTR)
			continue;
		if (!ISERR(r))
			break;
		PERR("1:SMTP:");perr_ipvx();PERR(":ERROR:%ld:error while reading from the tcp socket, trying next server\n", r);
		return false;
	}

	if (r == 0) {
		PERR("1:SMTP:");perr_ipvx();PERR(":WARNING:the tcp connection was shut down, trying next server\n");
		return false;
	}

	reply_data_e += r; /* r is min 1 */
	return true;
}

static bool reply_data_recv(void)
{
	/*
	 * 2 events if the timer do happen at the same time that some 
	 * gram is received
	 */
	struct epoll_event evts[2];
	sl evt;
	sl r;

	loop {		
		memset(evts, 0, sizeof(evts));
		r = epoll_pwait(epfd, evts, 2, -1, 0);
		if (r != -EINTR && r != 0)
			break;
		PERR("1:SMTP:");perr_ipvx();PERR(":WARNING:epoll_pwait was interruped by a signal or exited with 0 event, restarting\n");
	}
	if (ISERR(r)) {
		PERR("0:SMTP:");perr_ipvx();PERR(":ERROR:%ld:epoll_wait error\n", r);
		exit(1);
	}

	/* first: get some tcp data */
	evt = 0;
	loop {
		if (evt == r)
			break;
		if (evts[evt].data.fd == so) {
			if ((evts[evt].events & EPOLLIN) != 0) {
				if (!so_recv())
					return false;
				return true; /* have reply data to process */
			} else {
				PERR("0:SMTP:");perr_ipvx();PERR(":ERROR:got unselected event for the tcp socket, events 0x%08x\n", evts[evt].events);
				exit(1);
			}
			break;
		}
		++evt;
	}

	/* first: check the timer expiration if no reply data arrived */
	evt = 0;
	loop {
		if (evt == r)
			break;

		if (evts[evt].data.fd == timerfd) {
			if ((evts[evt].events & EPOLLIN) != 0) {
				if (timer_expired())
					return false;
			} else {
				PERR("0:SMTP:");perr_ipvx();PERR(":ERROR:got unselected event for the timer, events 0x%08x\n", evts[evt].events);
				exit(1);
			}
			break;
		}
		++evt;
	}
	PERR("0:SMTP:");perr_ipvx();PERR(":ERROR:unknown event\n");
	exit(1);
}

static void perr_reply_line(void)
{
	u8 *c;

	c = reply_line;
	PERR("1:SMTP:");perr_ipvx();PERR(":REPLY LINE:");
	loop {
		if (c == reply_line_e)
			break;
		if (is_print(*c))
			PERR("%c", *c);
		++c;
	}
	PERR("\n");
}

static void perr_cmd(void)
{
	u8 *c;

	c = cmd;
	PERR("1:SMTP:");perr_ipvx();PERR(":CMD:");
	loop {
		if (c == cmd_e)
			break;
		if (is_print(*c))
			PERR("%c", *c);
		++c;
	}
	PERR("\n");
}

#define HAVE_REPLY_LINE 0x01
#define NEED_MORE_DATA  0x02
#define NEXT_SERVER     0x03
static u8 reply_line_next(void)
{
	loop {
		if ((reply_line_e - reply_line) == SMTP_RFC_REPLY_LINE_N_MAX) {
			PERR("1:SMTP:");perr_ipvx();PERR(":WARNING:reply line too long, trying next server\n");
			return NEXT_SERVER;
		}

		if (reply_data_p == reply_data_e)
			return NEED_MORE_DATA;

		*reply_line_e = *reply_data_p;
		++reply_line_e;
		++reply_data_p;

		/* checking for the end of line */
		if (reply_line_e[-1] == '\n'
					&& &reply_line_e[-2] >= reply_line
						&& reply_line_e[-2] == '\r')
			return HAVE_REPLY_LINE;
	}
}

static bool reply_xxx(u8 *code, u64 timeout)
{
	u8 line;

	/* a full multiline reply must be received within this time */
	timer_start(timeout, 0);

	line = 0;
	reply_line_e = reply_line;
	loop {

		if (!reply_data_recv())
			return false;

		loop {
			u8 r;
			u16 reply_line_sz;

			if (line == CONFIG_SMTP_REPLY_LINES_N_MAX) {
				PERR("1:SMTP:");perr_ipvx();PERR(":WARNING:maximum number, %u, of reply lines in a multine reply reached, trying next server\n", CONFIG_SMTP_REPLY_LINES_N_MAX);
				return false;
			}

			r = reply_line_next();
			if (r == NEXT_SERVER)
				return false;
			else if (r == NEED_MORE_DATA)
				break;

			/* r == HAVE_REPLY_LINE */

			perr_reply_line();

			reply_line_sz = reply_line_e - reply_line;

			/*
			 * for a multiline reply we do check the code only
			 * on the last line.
			 * the major state is the line size.
			 */
			if (reply_line_sz < 3) {
				PERR("1:SMTP:");perr_ipvx();PERR(":WARNING:reply line too short to contain a code, trying next server\n");
				return false;
			}

			if (reply_line_sz == 3) {
				if (memcmp(reply_line, code, 3)) {
					return true;
				} else {
					PERR("1:SMTP:");perr_ipvx();PERR(":WARNING:wrong reply code from the server, expecting %s, trying next server\n", code);
					return false;
				}
			}

			/*
			 * reply_line_sz > 3
			 * 
			 * "1 liner with text" reply or last line of multine
			 * reply
			 */
			if (reply_line[3] != '-') {
				if (memcmp(reply_line, code, 3)) {
					return true;
				} else {
					PERR("1:SMTP:");perr_ipvx();PERR(":WARNING:wrong reply code from the server, expecting %s, trying next server\n", code);
					return false;
				}
			}

			/* multiline reply */

			++line;
			reply_line_e = reply_line;
		}
	}
}
#undef HAVE_REPLY_LINE
#undef NEED_MORE_DATA
#undef NEXT_SERVER

static void tcp_send_timeout_set(void)
{
	si timeout;
	si timeout_sz;
	sl r;

	/* backup the current value */
	timeout_sz = sizeof(so_user_timeout);
	r = getsockopt(so, IPPROTO_TCP, TCP_USER_TIMEOUT, &so_user_timeout,
								&timeout_sz);
	if (ISERR(r))
		{PERR("1:SMTP:");perr_ipvx();PERR(":WARNING:%ld:failed to get the tcp user timeout(size=%d), continuing anyway\n", r, timeout_sz);} 

	if (user_timeout_once) {
		PERR("1:SMTP:");perr_ipvx();PERR(":current tcp user timeout is %d ms (size is %d bytes)\n", so_user_timeout, timeout_sz);
		user_timeout_once = false;
	}

	timeout = SMTP_RFC_DATA_BLOCK_TIMEOUT_MSEC;
	r = setsockopt(so, IPPROTO_TCP, TCP_USER_TIMEOUT, &timeout,
							sizeof(timeout));
	if (ISERR(r))
		{PERR("1:SMTP:");perr_ipvx();PERR(":WARNING:%ld:failed to set the tcp user timeout to %d ms, continuing anyway\n", r, timeout);}
	
}

static void tcp_send_timeout_restore(void)
{
	sl r;

	r = setsockopt(so, IPPROTO_TCP, TCP_USER_TIMEOUT, &so_user_timeout,
						sizeof(so_user_timeout));
	if (ISERR(r))
		{PERR("1:SMTP:");perr_ipvx();PERR(":WARNING:%ld:failed to restore the tcp user timeout to %d ms, continuing anyway\n", r, so_user_timeout);}
}

static sl so_send(u8 *buf, u64 sz)
{
	sl r;
	u64 sent_bytes_n;

	tcp_send_timeout_set();

	sent_bytes_n = 0;
	loop {
		r = write(so, buf + sent_bytes_n, sz - sent_bytes_n);
		if (ISERR(r)) {
			/*
			 * we were told by epoll we can write to the socket,
			 * do insist
			 */
			if ((r == -EAGAIN) || (r == -EINTR))
				continue;
			break;
		}

		/* we force if r == 0 */

		sent_bytes_n += (u16)r;
		if (sent_bytes_n == sz)
			break;
	}

	tcp_send_timeout_restore();
	return r;
}

static bool send_cmd(void)
{
	sl r;
	u64 cmd_sz;

	cmd_sz = cmd_e - cmd;

	r = so_send(cmd, cmd_sz);
	if (ISERR(r)) {
		PERR("1:SMTP:");perr_ipvx();PERR(":WARNING:%ld:failed to send the command, trying next server\n", r); 
		return false;
	}
	PERR("1:SMTP:");perr_ipvx();PERR(":cmd sent, %u bytes\n", cmd_sz);
	return true;
}

static bool mail_send(void)
{
	sl r;

	r = so_send(email_v, email_sz_v);
	if (ISERR(r)) {	
		PERR("1:SMTP:");perr_ipvx();PERR(":WARNING:%ld:failed to send the mail, trying next server\n", r);
		return false;
	}
	PERR("1:SMTP:");perr_ipvx();PERR(":mail sent, %u bytes\n", email_sz_v);

	r = so_send(SMTP_RFC_TERMINATOR, CSTRLEN(SMTP_RFC_TERMINATOR));
	if (ISERR(r)) {
		PERR("1:SMTP:");perr_ipvx();PERR(":WARNING:%ld:failed to send the terminator sequence <CRLF>.<CRLF>, trying next server\n", r);
		return false;
	}
	PERR("1:SMTP:");perr_ipvx();PERR(":mail terminator sent, %u bytes\n", CSTRLEN(SMTP_RFC_TERMINATOR));
	return true;
}

static bool cmd_ehlo(void)
{
	#define CMD "EHLO " CONFIG_SMTP_EHLO_DN_AL "\r\n"
	memcpy(cmd, CMD, CSTRLEN(CMD));
	cmd_e = cmd + CSTRLEN(CMD);
	#undef CMD
	perr_cmd();
	return send_cmd();
}

static bool cmd_mail_from(void)
{
	#define CMD "MAIL FROM:<" CONFIG_SMTP_MAIL_FROM "@" \
						CONFIG_SMTP_EHLO_DN_AL ">\r\n"
	memcpy(cmd, CMD, CSTRLEN(CMD));
	cmd_e = cmd + CSTRLEN(CMD);
	#undef CMD
	perr_cmd();
	return send_cmd();
}

static bool cmd_rcpt(struct str_slice_t *rcpt)
{
	u64 len; /* trailing 0 is not accounted */
	/*
	 * XXX: our slice in actually pointing in the program arguments, which
	 * is a zero terminated string
	 */
	#define CMD "RCPT TO:<%s>\r\n"
	len = snprintf(cmd, SMTP_RFC_COMMAND_LINE_N_MAX, CMD, rcpt->s);
	#undef CMD
	cmd_e = cmd + len;
	perr_cmd();
	return send_cmd();
}

static bool cmd_data(void)
{
	#define CMD "DATA\r\n"
	memcpy(cmd, CMD, CSTRLEN(CMD));
	cmd_e = cmd + CSTRLEN(CMD);
	#undef CMD
	perr_cmd();
	return send_cmd();
}

static bool cmd_quit(void)
{
	#define CMD "QUIT\r\n"
	memcpy(cmd, CMD, CSTRLEN(CMD));
	cmd_e = cmd + CSTRLEN(CMD);
	#undef CMD
	perr_cmd();
	return send_cmd();
}

/* this is our mail transaction with a specific server */
static bool server_send(void)
{
	bool r;
	u8 rcpt;
	
	r = true;

	if (!tcp_connect()) {
		r = false;
		goto exit;
	}

	/* from here, we have a connected tcp socket to the server */

	if (!reply_xxx("220", SMTP_RFC_INITIAL_REPLY_TIMEOUT_SEC)) {
		r = false;
		goto shutdown_so;
	}

	if (!cmd_ehlo()) {
		r = false;
		goto shutdown_so;
	}

	if (!reply_xxx("250", SMTP_RFC_INITIAL_REPLY_TIMEOUT_SEC)) {
		r = false;
		goto shutdown_so;
	}

	if (!cmd_mail_from()) {
		r = false;
		goto shutdown_so;
	}

	if (!reply_xxx("250", SMTP_RFC_COMMAND_MAIL_FROM_REPLY_TIMEOUT_SEC)) {
		r = false;
		goto shutdown_so;
	}

	rcpt = 0;
	loop {
		if (rcpt == dn_al->local_parts_n)
			break;
		if (!cmd_rcpt(&dn_al->local_parts[rcpt])) {
			r = false;
			goto shutdown_so;
		}

		if (!reply_xxx("250",
				SMTP_RFC_COMMAND_RCPT_TO_REPLY_TIMEOUT_SEC)) {
			r = false;
			goto shutdown_so;
		}

		++rcpt;
	}

	if (!cmd_data()) {
		r = false;
		goto shutdown_so;
	}

	if (!reply_xxx("354", SMTP_RFC_COMMAND_DATA_REPLY_TIMEOUT_SEC)) {
		r = false;
		goto shutdown_so;
	}

	if (!mail_send()) {
		r = false;
		goto shutdown_so;
	}

	if (!reply_xxx("250", SMTP_RFC_DATA_TERMINATION_REPLY_TIMEOUT_SEC)) {
		r = false;
		goto shutdown_so;
	}

	/*
	 * XXX: just in case some nasty smtp servers would cancel our email
	 * transaction without a proper quit/221 sequence, but since smtp is
	 * based upon an underlying connected protocol, the tcp termination
	 * should be enough. the smtp protocol rfc should have used
	 * basic "underlying connected protocol operations/events" like
	 * timeout/disconnect.
	 */

	if (!cmd_quit()) {
		r = false;
		goto shutdown_so;
	}

	if (!reply_xxx("221", SMTP_RFC_COMMAND_DATA_REPLY_TIMEOUT_SEC))
		r = false;

	PERR("0:SMTP:");perr_ipvx();PERR(":mail sent\n"); 

shutdown_so:
	so_shutdown();
exit:
	return r;
}

static void servers_send(void)
{
	u8 i;

	i = 0;
	loop {
		if (i == dn_al->smtp_ips_n)
			return;

		ip = &dn_al->smtp_ips[i];
		if (server_send())
			break;
		++i;
	}
}

static void timer_setup(void)
{
	sl r;
	struct epoll_event evts;

	r = timerfd_create(CLOCK_MONOTONIC, TFD_NONBLOCK);
	if (ISERR(r)) {
		PERR("0:SMTP:ERROR:%ld:unable to create the timer file descriptor\n", r);
		exit(1);
	}
	timerfd = (si)r;

	/* add the timerfd to the epollfd */
	memset(&evts, 0, sizeof(evts));
	/* could be EPOLLET since we deal only with 1 expiration */
	evts.events = EPOLLIN;
	evts.data.fd = timerfd;
	r = epoll_ctl(epfd, EPOLL_CTL_ADD, timerfd, &evts);
	if (ISERR(r)) {
		PERR("0:SMTP:ERROR:%ld:unable to add the timer file descriptor to the epoll file descriptor\n", r);
		exit(1);
	}
}
/******************************************************************************/
/* exported in smtp.h */
static void smtp_send(void)
{
	u64 d;

	d = 0;
	loop {
		if (d == dn_al_n_v)
			break;

		dn_al = &dn_al_v[d];

		servers_send();
		++d;
	}
}

static void smtp_init(void)
{
	sl r;

	cmd_e = cmd;
	user_timeout_once = true;

	r = epoll_create1(0);
	if (ISERR(r)) {
		PERR("0:SMTP:ERROR:%ld:unable to create the epoll file descriptor\n", r);
		exit(1);
	}
	epfd = (si)r;
	timer_setup();
}
/*----------------------------------------------------------------------------*/
#define CLEANUP
#include "namespace/ulinux.h"
#include "namespace/syncsm.h"
#include "namespace/smtp.h"
#include "smtp/namespace/smtp.c"
#undef CLEANUP
#endif
