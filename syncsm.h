#ifndef SYNCSM_H
#define SYNCSM_H
/*
 * this code is protected by the GNU affero GPLv3 license
 * author:Sylvain BERTRAND
 */
#include "namespace/ulinux.h"
#include "namespace/syncsm.h"
/*----------------------------------------------------------------------------*/
struct str_slice_t {
	u8 *s; /* first valid char */
	u8 *l; /* last valid char */
};

#if defined CONFIG_SMTP_IPV4 || defined CONFIG_DNS_IPV4
enum {/* maybe too generic for ip type constants */
	ipv6 = 0x01,
	ipv4 = 0x02
};
#endif
struct ip_t {
#if defined CONFIG_SMTP_IPV4 || defined CONFIG_DNS_IPV4
	u8 type;
#endif
	union {
		/* net endian, aka big endian */
#if defined CONFIG_SMTP_IPV4 || defined CONFIG_DNS_IPV4
		u32 ipv4_net;
#endif
		union { /* different ways to get into an ipv6 addr*/
			u8 ipv6_net[16];
			struct {
				u64 ipv6_net_h;
				u64 ipv6_net_l;
			} PACKED;

		};
	};
};
/*----------------------------------------------------------------------------*/
/*
 * (domain name | address literal) index:
 * for each (domain name | address literal), we'll have the list of recipients
 */
enum {
	literal_ip	= 0x01,
	domain_name	= 0x02
};
struct dn_al_t {
	u8 type;
	struct str_slice_t dn;

	struct str_slice_t local_parts[SMTP_RFC_RCPTS_N_MAX];
	u8 local_parts_n;

	/*
	 * literal ip: smtp_ips[0] is the ip.
	 * domain name: ordered (based on mx preference if existing) smtp
	 *              ips
	 */
	struct ip_t smtp_ips[CONFIG_DN_SMTP_IPS_N_MAX];
	u8 smtp_ips_n;
};
static struct dn_al_t dn_al_v[CONFIG_DN_AL_N_MAX];
static u64 dn_al_n_v;
/*----------------------------------------------------------------------------*/
/*
 * aarch64 gcc/binutils(6.3.0/2.28) is unable to deal with very large 
 * bss sections. we have to mmap manually.
 */
static u8 *email_v;
static u64 email_sz_v;
/*----------------------------------------------------------------------------*/
#define CLEANUP
#include "namespace/ulinux.h"
#include "namespace/syncsm.h"
#undef CLEANUP
#endif
