#ifndef ULINUX_ARCH_MMAP_H
#define ULINUX_ARCH_MMAP_H
/*
 * this code is protected by the GNU affero GPLv3
 * author:Sylvain BERTRAND
 */
#ifndef ULINUX_PAGE_SHIFT
#error "ulinux:you must define ULINUX_PAGE_SHIFT macro to 12(page size=4KiB, the most common) or 14(page size=16KiB) or 16(page size=64KiB) because it depends on your linux build configuration"
#endif
#define ULINUX_PAGE_SZ (1 << ULINUX_PAGE_SHIFT)
#endif
