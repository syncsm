#include <stdarg.h>

#include <ulinux/compiler_types.h>
#include <ulinux/compiler_misc.h>
#include <ulinux/sysc.h>
#include <ulinux/types.h>
#include <ulinux/epoll.h>
#include <ulinux/file.h>
#include <ulinux/error.h>
#include <ulinux/signal/signal.h>

#include <ulinux/utils/mem.h>
#include <ulinux/utils/ascii/string/string.h>
#include <ulinux/utils/ascii/string/vsprintf.h>

/* ulinux namespace */
#define EINTR ULINUX_EINTR
#define EAGAIN ULINUX_EAGAIN
#define si ulinux_si
#define sl ulinux_sl
#define u8 ulinux_u8
#define u64 ulinux_u64
#define exit(code) ulinux_sysc(exit_group,1,code)
#define rt_sigprocmask(a,b,c,d) ulinux_sysc(rt_sigprocmask,4,a,b,c,d)
#define SIG_BLOCK ULINUX_SIG_BLOCK
#define SIGUSR1 ULINUX_SIGUSR1
#define SIGUSR2 ULINUX_SIGUSR2
#define ISERR ULINUX_ISERR
#define signalfd4(a,b,c,d) ulinux_sysc(signalfd4,4,a,b,c,d)
#define SFD_NONBLOCK ULINUX_SFD_NONBLOCK
#define epoll_create1(a) ulinux_sysc(epoll_create1,1,a)
#define epoll_event ulinux_epoll_event
#define memset(a,b,c) ulinux_memset((ulinux_u8*)a,b,c)
#define EPOLLET ULINUX_EPOLLET
#define EPOLLIN ULINUX_EPOLLIN
#define epoll_ctl(a,b,c,d) ulinux_sysc(epoll_ctl,4,a,b,c,d)
#define EPOLL_CTL_ADD ULINUX_EPOLL_CTL_ADD
#define epoll_wait(a,b,c,d) ulinux_sysc(epoll_wait,4,a,b,c,d)
#define signalfd_siginfo ulinux_signalfd_siginfo
#define read(a,b,c) ulinux_sysc(read,3,a,b,c)
#define getpid() ulinux_sysc(getpid,0)

/* convenience macros */
#define BUFSIZ 8192
static u8 dprint_buf[BUFSIZ];
#define POUT(fmt,...) ulinux_dprintf(1, &dprint_buf[0], BUFSIZ - 1, fmt, ##__VA_ARGS__)
#define SIGBIT(sig) (1 << (sig - 1))
#define EPOLL_EVENTS_N 10
#define loop for(;;)

void _start(void)
{
	sl pid;
	u64 mask;
	sl r;
	si sigs_fd;
	si epfd;
	struct epoll_event evts[EPOLL_EVENTS_N];
	u8 *illegal;

	dprint_buf[BUFSIZ - 1] = 0; /* secure a 0 terminating char */
	illegal = 0;

	pid = getpid();
	POUT("PID=%ld\n", pid);

	mask = ~0;
	r = rt_sigprocmask(SIG_BLOCK, &mask, 0, sizeof(mask));
	if (ISERR(r)) {
		POUT("\nunable to block 'all' signals for synchronous delivery\n");
		exit(-1);
	}

	mask = SIGBIT(SIGUSR1) | SIGBIT(SIGUSR2);
	sigs_fd = (si)signalfd4(-1, &mask, sizeof(mask), SFD_NONBLOCK);
	if (ISERR(sigs_fd)) {
		POUT("\nunable to create the signalfd\n");
		exit(-2);
	}

	epfd = (si)epoll_create1(0);
	if (ISERR(epfd)) {
		POUT("\nunable to create the epoll fd\n");
		exit(-3);
	}

	memset(evts, 0, sizeof(evts));
	evts[0].events = EPOLLET | EPOLLIN;
	evts[0].data.fd = sigs_fd;
	r = epoll_ctl(epfd, EPOLL_CTL_ADD, sigs_fd, &evts[0]);
	if (ISERR(r)) {
		POUT("\nunable to add the signalfd to the epoll fd\n");
		exit(-4);
	}

	loop {
		sl j;

		loop {		
    			memset(evts, 0, sizeof(evts));
			r = epoll_wait(epfd, evts, EPOLL_EVENTS_N, -1);
			if (r != -EINTR)
				break;
	
			POUT("\nepoll_wait was interruped by a signal, but got nothing in the signalfd: MUST BE SIGSTOP/SIGCONT\n");
		}
		if (ISERR(r)) {
			POUT("epoll_wait error\n");
			exit(-6);
		}

		j = 0;
		loop {
			if (j == r)
				break;
			if (evts[j].data.fd == sigs_fd) {
				if ((evts[j].events & EPOLLIN) != 0) {
					struct signalfd_siginfo info;

					loop {
						memset(&info, 0, sizeof(info));
						loop {/* reads are atomic or err, aka no short reads */
							r = read(sigs_fd, &info, sizeof(info));
							if (r != -EINTR);
								break;
						}
						if ((r != -EAGAIN) && (ISERR(r))) {
							POUT("\nsomething went wront while getting signal information\n");
							exit(-7);
						}

						switch (info.ssi_signo) {
						case SIGUSR1:
							POUT("\nSIGUSR1:generating illegal access\n");
							*illegal = 0;
							break;
						case SIGUSR2:
							POUT("\nSIGUSR2\n");
							break;
						}
					}
				} else {
					POUT("\ngot an unwanted event\n");
					exit(-8);
				}
			}
			++j;
		}
	}
}
