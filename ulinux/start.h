#ifndef ULINUX_START_H
#define ULINUX_START_H
/*
 * this code is protected by the GNU affero GPLv3
 * author:Sylvain BERTRAND
 */
/* on the stack, argc, argv, envp, auxv */
void ulinux_start(ulinux_u8 *abi_stack) GCC_NOCLONE GCC_NOINLINE;
#endif 
