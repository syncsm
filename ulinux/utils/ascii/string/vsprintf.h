#ifndef ULINUX_UTILS_ASCII_STRING_VSPRINTF_H
#define ULINUX_UTILS_ASCII_STRING_VSPRINTF_H
/*
 * this code is protected by the GNU affero GPLv3
 * author:Sylvain BERTRAND
 */

/*----------------------------------------------------------------------------*/
/* "One Compilation Unit" support */
#ifdef ULINUX_UTILS_EXTERNAL
#define ULINUX_EXPORT extern 
#else
#define ULINUX_EXPORT static
#endif
/*----------------------------------------------------------------------------*/

ULINUX_EXPORT ulinux_u64 ulinux_vsnprintf(ulinux_u8 *buf, ulinux_u64 sz,
						ulinux_u8 *fmt, va_list args);
ULINUX_EXPORT ulinux_u64 ulinux_snprintf(ulinux_u8 *buf, ulinux_u64 sz,
							ulinux_u8 *fmt, ...);
ULINUX_EXPORT bool ulinux_dprintf(ulinux_si f, ulinux_u8 *buf, ulinux_u64 sz,
							ulinux_u8 *fmt, ...);
#undef ULINUX_EXPORT
#endif
