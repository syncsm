#ifndef ULINUX_UTILS_MEM_C
#define ULINUX_UTILS_MEM_C
/*
 * this code is protected by the GNU affero GPLv3
 * author:Sylvain BERTRAND
 */
#include <stdbool.h>
#include <stddef.h> /* size_t */

#include <ulinux/compiler_types.h>
#include <ulinux/types.h>
/*----------------------------------------------------------------------------*/
/* "One Compilation Unit" support */
#ifdef ULINUX_UTILS_EXTERNAL
#define ULINUX_EXPORT
#else
#define ULINUX_EXPORT static
#endif
/*----------------------------------------------------------------------------*/
/* local */
#define loop for(;;)
/*******************************************************************************
 * XXX: DO NOT USE GCC BUILTINS! Got tons of weird bugs with those specific
 * builtins (gcc 7.3.0)
 ******************************************************************************/
ULINUX_EXPORT void ulinux_memcpy(void *d, void *s, ulinux_u64 len)
{
	ulinux_u8 *to;
	ulinux_u8 *from;
	
	to = d;
	from = s;
	loop {
		if (len == 0)
			break;
		*to++ = *from++;
		len--;
	}
}

ULINUX_EXPORT void ulinux_memset(void *d, ulinux_u8 c, ulinux_u64 len)
{
	ulinux_u8 *cur;

	cur = d;
	loop {
		if (len == 0)
			break;
		*cur++ = c;
		len--;
	}
}

ULINUX_EXPORT bool ulinux_memcmp(void *d, void *c, ulinux_u64 len)
{
	ulinux_u8 *su1;
	ulinux_u8 *su2;

	su1 = d;
	su2 = c;
	loop {
		if (len == 0)
			return true;

		if (*su1 != *su2)
			return false;
			
		++su1;
		++su2;
		len--;
	}
}
/*----------------------------------------------------------------------------*/
#undef loop
#undef ULINUX_EXPORT
/*----------------------------------------------------------------------------*/
#endif
