#ifndef ULINUX_UTILS_MEM_H
#define ULINUX_UTILS_MEM_H
/*
 * this code is protected by the GNU affero GPLv3
 * author:Sylvain BERTRAND
 */
/*----------------------------------------------------------------------------*/
/* "One Compilation Unit" support */
#ifdef ULINUX_UTILS_EXTERNAL
#define ULINUX_EXPORT extern
#else
#define ULINUX_EXPORT static
#endif
/*----------------------------------------------------------------------------*/
ULINUX_EXPORT void ulinux_memcpy(void *d, void *s, ulinux_u64 len);
ULINUX_EXPORT void ulinux_memset(void *d, ulinux_u8 c, ulinux_u64 len);
ULINUX_EXPORT bool ulinux_memcmp(void *s1, void *s2, ulinux_u64 len);
#undef ULINUX_EXPORT
#endif
